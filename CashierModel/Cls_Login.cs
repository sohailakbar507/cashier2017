﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashierModel
{
    public class Cls_Login
    {
        public string fyear { get; set; }
        public int fyearid { get; set; }
        public DateTime yearstart { get; set; }
        public DateTime yearend { get; set; }
        private string _Username;// Member variables
        private string _SubcatId;
        
        //public string UserName { get; set; }

        public string Username // Properties 
        {
            get { return _Username; }
            set { _Username = value; }
        }
        public string SubcatId // Properties 
        {
            get { return _SubcatId; }
            set { _SubcatId = value; }
        }


        private int _userid;

        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private int _Groupid;

        public int Groupid
        {
            get { return _Groupid; }
            set { _Groupid = value; }
        }

        private Boolean _Isedit;

        public Boolean Isedit
        {
            get { return _Isedit; }
            set { _Isedit = value; }
        }
        private Boolean _Isadd;

        public Boolean Isadd
        {
            get { return _Isadd; }
            set { _Isadd = value; }
        }
        private Boolean _Isdel;

        public Boolean Isdel
        {
            get { return _Isdel; }
            set { _Isdel = value; }
        }

        public Boolean IsUpdateForm // Auto Properties
        {
            get;
            set;
        }

    }
}
