//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CashierModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class payroll
    {
        public int Emp_Id { get; set; }
        public string srno { get; set; }
        public string persnno { get; set; }
        public string venderno { get; set; }
        public string offname { get; set; }
        public string desg { get; set; }
        public string bs { get; set; }
        public Nullable<double> bp { get; set; }
        public Nullable<double> gross { get; set; }
        public Nullable<double> offcode { get; set; }
        public Nullable<double> offid { get; set; }
        public Nullable<double> empid { get; set; }
        public string section { get; set; }
        public string i { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string phnores { get; set; }
        public string phnooff { get; set; }
        public string mbno { get; set; }
        public string desg1 { get; set; }
    }
}
