//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CashierModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class expense
    {
        public int exp_id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string head { get; set; }
        public Nullable<double> unit { get; set; }
        public string descr { get; set; }
        public string descr2 { get; set; }
        public Nullable<double> amount { get; set; }
        public Nullable<double> amnt { get; set; }
        public Nullable<double> services { get; set; }
        public string amtinwords { get; set; }
        public Nullable<double> taxrate { get; set; }
        public Nullable<double> itax { get; set; }
        public Nullable<double> stax { get; set; }
        public Nullable<double> tax { get; set; }
        public Nullable<double> diff { get; set; }
        public Nullable<double> qty { get; set; }
        public Nullable<int> empid { get; set; }
        public Nullable<int> offcode { get; set; }
        public Nullable<System.DateTime> doapp { get; set; }
        public Nullable<int> partycode { get; set; }
        public Nullable<int> letcode { get; set; }
        public string bilrefence { get; set; }
        public Nullable<System.DateTime> dobill { get; set; }
        public string billno { get; set; }
        public Nullable<double> itemamount { get; set; }
        public Nullable<double> billamount { get; set; }
        public Nullable<double> advpayment { get; set; }
        public Nullable<double> payment { get; set; }
        public string cbno { get; set; }
        public Nullable<System.DateTime> dosubtoag { get; set; }
        public string tokenno { get; set; }
        public Nullable<System.DateTime> doobj { get; set; }
        public string chqno { get; set; }
        public Nullable<double> chamount { get; set; }
        public Nullable<System.DateTime> dopayment { get; set; }
        public string remarks { get; set; }
        public Nullable<double> status { get; set; }
        public Nullable<double> bf { get; set; }
        public Nullable<double> gst { get; set; }
        public Nullable<double> ded_intex { get; set; }
        public Nullable<double> ded_gst { get; set; }
        public string category { get; set; }
        public Nullable<double> srno { get; set; }
        public string name { get; set; }
        public Nullable<int> offid { get; set; }
        public Nullable<double> unitprice { get; set; }
        public Nullable<bool> agstatus { get; set; }
        public string Sanc_id { get; set; }
        public string fyear { get; set; }
        public string UserName { get; set; }
        public Nullable<double> taxrate10 { get; set; }
        public Nullable<double> itax10 { get; set; }
    }
}
