﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace CashierModel
{
    public class Cls_Encrypt
    {
        static byte[] key = null;
        static byte[] iv = { 1, 2, 3, 4, 5, 6, 7, 8 };

        public static string Encrypt(string Value)
        {
            DESCryptoServiceProvider desSProvider = new DESCryptoServiceProvider();
            Encoding utf = new UTF8Encoding();
            key = utf.GetBytes("sz83kd75");
            ICryptoTransform encryptor = desSProvider.CreateEncryptor(key, iv);
            byte[] bValue = utf.GetBytes(Value);
            byte[] bEncrypt = encryptor.TransformFinalBlock(bValue, 0, bValue.Length);
            return Convert.ToBase64String(bEncrypt);
        }

        public static string Decrypt(string Value)
        {
            DESCryptoServiceProvider desSProvider = new DESCryptoServiceProvider();
            Encoding utf1 = new UTF8Encoding();


            key = utf1.GetBytes("sz83kd75");
            ICryptoTransform decryptor = desSProvider.CreateDecryptor(key, iv);
            Encoding utf = new UTF8Encoding();
            Value = Value.Replace(" ", "+").Replace("'", "");
            byte[] bEncrypt = Convert.FromBase64String(Value);
            byte[] bDecrypt = decryptor.TransformFinalBlock(bEncrypt, 0, bEncrypt.Length);
            return utf.GetString(bDecrypt);
        }

        public static int fanos;
        public static int fanod;



        public string GetDate(DateTime Stdr)
        {
            string year;
            string Month;
            string Day;

            string Date;

            year = Convert.ToInt32(Stdr.Year).ToString();
            Month = Convert.ToInt32(Stdr.Month).ToString();
            Day = Convert.ToInt32(Stdr.Day).ToString();


            if (Month.Length == 1)
            {
                Month = "0" + Month;
            }

            if (Day.Length == 1)
            {
                Day = "0" + Day;
            }


            Date = (year + "-" + Month + "-" + Day).ToString();

            return Date;
        }
    }

}

