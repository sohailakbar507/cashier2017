﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="UtilityBills.aspx.cs" Inherits="Azad.UtilityBills" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function FillBilling(f) {
        $("#ContentPlaceHolder1_officername").val(f);
    }
  function SelectParty(f) {
            $("#ContentPlaceHolder1_txtpartycode").val(f);
        }
      
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="panel panel-default">

                        <div class="panel-heading">
                        <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="Utility Bills Entry Form"></asp:Label>
                        </h5>
                        </div>
                        
                        <div class="panel-body">
       <div>

       <table align="center" width="100%">
            
            <tr>
                <td><asp:Label ID="lblHead" runat="server" Text="Head"></asp:Label></td> 
                <td><asp:DropDownList ID="DDLHead" runat="server" 
                onselectedindexchanged="DDLHead_SelectedIndexChanged" Width="300px" CssClass="form-control" 
                AutoPostBack="True">
                </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="DDLHead" ErrorMessage="Enter Head" 
                        ValidationGroup="1" ForeColor="Red">*</asp:RequiredFieldValidator></td>                 
                
                
            </tr>
            <tr>
                <td><asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label></td> 
                <td><asp:TextBox ID="txtUnit" runat="server" Width="300px"  
                CssClass="form-control" ToolTip="Self"></asp:TextBox></td>
                <td><asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label></td> 
                <td><asp:TextBox ID="txtAmount" runat="server" Width="300px"  
                CssClass="form-control" ToolTip="Self"></asp:TextBox></td>
            </tr>
                              
             <tr>
                <td><asp:Label ID="lblOffCode" runat="server" Text="Office Code"></asp:Label></td>
                <td>  
                <div class="row">
                <div class="col-lg-6">
                <div class="input-group">
                  <%--<input type="text" runat="server" id="officername" disabled="disabled" class="form-control" placeholder="Offcode..." />--%>
                  <asp:TextBox ID="officername" runat="server" Width="190px"  CssClass="form-control"></asp:TextBox>
                  <span class="input-group-btn">
                       <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" data-keyboard="false" data-backdrop="static" style="cursor:pointer" type="button">Select Office !</button>
                  </span>
    </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
    </div> </td>
                <td><asp:Label ID="lblCbNo" runat="server" Text="CB #"></asp:Label></td>
                <td><asp:TextBox ID="txtCBNo" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                </td>
    
    </tr>
            <tr>
                <td><asp:Label ID="lblBillRef" runat="server" Text="Bill Reference"></asp:Label></td>
                <td><asp:TextBox ID="txtBillRef" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                </td>
                <td><asp:Label ID="lblBillDate" runat="server" Text="Bill Date"></asp:Label></td>
                <td><telerik:RadDateTimePicker ID="BillDate" runat="server" Width="300px" CssClass="form-control">                   
                </telerik:RadDateTimePicker>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="lblName" runat="server" Text="Officer Name"></asp:Label></td>
                <td><asp:TextBox ID="txtName" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                </td>
                <td><asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></td>
                <td><asp:TextBox ID="txtRemarks" runat="server" Width="300px" 
                        CssClass="form-control" TextMode="MultiLine"> </asp:TextBox>
                </td>
                            
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click1" class="btn btn-primary btn-lg" Width="85px" 
                        Text="Save" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    
                    <asp:GridView ID="GVListShow" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="4" 
                        ForeColor="#333333" GridLines="Vertical" PageSize="5"  
                        onrowcancelingedit="GVListShow_RowCancelingEdit1" 
                        onrowediting="GVListShow_RowEditing" 
                        onrowupdating="GVListShow_RowUpdating" 
                         onselectedindexchanging="GVListShow_SelectedIndexChanging" 
                        onrowcommand="GVListShow_RowCommand" ViewStateMode="Enabled" 
                        >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Exp_Id" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                         <asp:TemplateField HeaderText="Head">
                             <ItemTemplate>
                                <asp:Label ID="lblhead" runat="server" Text= '<%#Eval("head")%>'></asp:Label>
                         </ItemTemplate>
                             <ControlStyle Width="80px" />
                             <HeaderStyle Height="70px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Unit">
                            <ItemTemplate>
                                <asp:Label ID="lblUnit" runat="server" Text= '<%#Eval("unit")%>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="60px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text= '<%#Eval("amount")%>'></asp:Label>
                            </ItemTemplate>
                            
                                <ItemStyle HorizontalAlign="Center" />
                            
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="OffCode">
                            <ItemTemplate>
                                 <asp:Label ID="lblOffCode" runat="server" Text= '<%#Eval("offcode")%>'></asp:Label>
                            </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CB #">
                               <ItemTemplate>
                                 <asp:Label ID="lblcbNo" runat="server" Text= '<%#Eval("cbno")%>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bill Ref.">
                            <ItemTemplate>
                                <asp:Label ID="lblBillRef" runat="server" Text= '<%#Eval("bilrefence")%>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DoBill">
                            <ItemTemplate>
                                <asp:Label ID="lbldoBill" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OfficerName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text= '<%#Eval("name")%>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="150px" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text= '<%#Eval("remarks")%>'></asp:Label>
                            </ItemTemplate>
                                <ControlStyle Width="120px" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sanc_Id">
                            <ItemTemplate>
                                 <asp:Label ID="lblSancId" runat="server" Text= '<%#Eval("Sanc_id")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DOSubToAG">
                            <ItemTemplate>
                                <asp:Label ID="lbldoSubtoAG" runat="server" Text='<%#Eval("dosubtoag","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                            <%--<asp:LinkButton ID="lnkbtnEdit" CommandName="Update" runat="server"></asp:LinkButton>--%>
                                    <asp:LinkButton ID="LinkButton1" CommandName="edit" CommandArgument='<%#Eval("exp_id") %>' runat="server"><img src="Images/graphic_design.png" border = "0" alt = "" /></asp:LinkButton>
                            </ItemTemplate>
                            <ControlStyle ForeColor="#006600" Font-Bold="False" Font-Size="Medium" 
                             Width="50px" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />

                            
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cancel">
                            <ItemTemplate>
                            <%--<asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server"> <img 
                                    src="Images/stop.png" border = "0" alt = "" /> </asp:LinkButton>--%>
                                <asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server"><img 
                                    src="Images/stop.png" border = "0" alt = "" /></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                                    
                            </asp:TemplateField>
                            
                        </Columns>
                        <EditRowStyle BackColor="#7C6F57" />
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#E3EAEB" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>
                    
                </td>
            </tr>


        </table>

    </div>
    </div> </div>
     <div class="modal fade bs-example-modal-lg" id="parties" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel1">Select Party</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">

                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table1">
                                    <thead>
                                        <tr>
                                            <th>Party Code</th>
                                            <th>Party Name</th>
                                            <th>Ph #</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%

                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            var officers = (from a in ent.v_AdminStaff select a).ToList();
                                            var Parties = (from a in ent.parties select a).ToList();

                                            foreach (var abc in Parties)
                                            { %>
                                        <tr>
                                            <td><%=abc.partycode%></td>
                                            <td><%=abc.name%></td>
                                            <td><%=abc.phno%></td>
                                            <td><span class="input-group-btn">
                                                <button id="selectoff1" class="btn btn-default" onclick="SelectParty(<%=abc.partycode %>)" type="button" data-dismiss="modal">Select!</button>
                                            </span></td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Select Office</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <div class="form-group">
               
               <div class="dataTable_wrapper">
                                
                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Employee Name</th>
                                            <th>Designation</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <%
                                        
                                            foreach (var abc in officers)
                                            { %>
                                    <tr>
                                    <td><%=abc.EmployeeId%></td>
                                    <td><%=abc.EmployeeName%></td>
                                    <td><%=abc.Desg%></td>
                                    <td> <span class="input-group-btn">
        <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.EmployeeId %>)" type="button" data-dismiss="modal">Select!</button>
        </span></td>
                                    </tr>
                                    <%}
                                        %>
                                    </tbody>
                                    </table>

         </div>
         </div>
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
        
  </div>
</asp:Content>
