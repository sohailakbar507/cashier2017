﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="TeleReportForm.aspx.cs" Inherits="Azad.TeleReportForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
</telerik:RadScriptManager>

<div style="width: 100%; height: 100%;">
        <div style="background-color: #f7f7f9; border: 1px solid #e1e1e8; width:100%; height: 500px; position:relative; left: 0px;top: 100px;">
        
                
                 <div class="row">
                 <div class="col-md-12" style= "left:200px">
                    <h1> CB Wise Print </h1>
                </div>
                <div class="row">
                   <div class="col-md-12" style= "left:200px; position:relative; top: 50px;">
                       Reference/CB #
                   </div>
                </div></div>
                <div class="row">
                    <div class="col-md-12" style= "left:200px; position:relative; top: 50px;">
                        <asp:TextBox ID="txtCBNo" Width="250" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                     <div class="col-md-12" style= "left:200px; position:relative; top: 50px;">
                         <asp:TextBox ID="txtFYear" Width="250" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-12" style= "left: 250px; position:relative; top: 100px;">
                     <asp:Button ID="btnPreview" class="btn btn-primary btn-lg" runat="server" 
                     Text="Preview" onclick="btnPreview_Click" />
                </div>
                </div>
                </div> <br /><br />
                         
</div>
</asp:Content>
