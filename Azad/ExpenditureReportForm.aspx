﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpenditureReportForm.aspx.cs" Inherits="Azad.ExpenditureReportForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Sohail Akbar">
     <!-- Bootstrap Core CSS -->
    <link  href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Timeline CSS -->
    <link  href="dist/css/timeline.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link  href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
     <!-- date picker-->
     <link href="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
    <!--end Date Pickers-->
        <style>
        body {
    background: url('/Images/cool_bg1.jpg') no-repeat, no-repeat; 
    background-repeat: no-repeat;
    background-position: center center;
    background-attachment: fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
            </style>
    </head>
      <script type="text/javascript">
          function DatePickerf() {
              jQuery('.mydatepicker, #datepicker').datepicker(
                  {
                      autoclose: true
                      , todayHighlight: true,
                      format: 'dd-mm-yyyy'
                  });
          }
    </script>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>   
        
      <br /><br />
                      <div class="container">
                      <div class="row">
                      <div class="Absolute-Center is-Responsive">
                      <div class="panel panel-danger">
                      <div class="panel-heading panel-primary">
                        
                            <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="EXPENDITURE REPORT FORM" ForeColor="blue"></asp:Label>
                            </h5> 
                             
                        </div>
                           <script type="text/javascript" language="javascript">
                                Sys.Application.add_load(DatePickerf);
                            </script>   
                            <div class="row" align="center">                                           
                            
                             <asp:Label ID="lblFromDate" runat="server" Text="From Date" Font-Size="Medium" ForeColor="#000066"></asp:Label>
                             <asp:TextBox ID="dtpFromDate" aria-describedby="sizing-addon2" CssClass="form-control mydatepicker" Width="250px"  placeholder="dd-MM-yyyy" runat="server"></asp:TextBox>
                            <%-- <span class="input-group-addon glyphicon glyphicon-calendar"></span>--%>
                                                  
                             <asp:Label ID="lblToDate" runat="server" Text="Todate" Font-Size="Medium" ForeColor="#000066"></asp:Label>
                      
                         <asp:TextBox ID="dtpToDate" CssClass="form-control mydatepicker" Width="250px" placeholder="dd-MM-yyyy" runat="server"></asp:TextBox>
                          <br /><br />
                                 <asp:Button ID="btnShow" align="center" runat="server" Text="PRINT PREVIEW" CssClass="btn btn-primary btn-danger" onclick="btnSubmit_Click" />

                            </div>
                            </div></div></div></div>
                           
    <!-- jQuery -->
    <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


    <!-- Morris Charts JavaScript -->
    <script type="text/javascript" src="../bower_components/raphael/raphael-min.js"></script>
    <script type="text/javascript" src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="JS/Menu-Collapse.js"></script>
            <!-- Date Picker Plugin JavaScript -->
    <script type="text/javascript" src="bower_components/moment/moment.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        </div>
    </form>
</body>
</html>
