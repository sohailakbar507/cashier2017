﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AttendanceFormCMS.aspx.cs" Inherits="Azad.AttendanceFormCMS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <div id="deldialog" style="display: none" align="center">
    Do you want to delete this record?
</div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 align="center">&nbsp;<asp:Label ID="lblTitle" runat="server" Text="ATTENDANCE FORM CMS OFFICE"></asp:Label>
            </h3>
        </div>

        <div class="panel-body">
            <div>

                <table align="center" width="100%">

                     <tr>
                        <td>
                            <asp:Label ID="lblOffCode" runat="server" Text="Office Code"></asp:Label></td>
                        <td>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <%--<input type="text" runat="server" id="officername" disabled="disabled" class="form-control" placeholder="Offcode..." />--%>
                                        <asp:TextBox ID="officername" runat="server" Width="190px" CssClass="form-control"></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-toggle="modal" data-target="#officers" data-keyboard="false" data-backdrop="static" style="cursor: pointer" type="button">Select Officer !</button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                        </td>

                         <td>
                             
                         <asp:DropDownList ID="DDLeaveType" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:DropDownList>
                         </td>
                     </tr>

                    <tr>
                        
                        <td>
                            <asp:Label ID="lblEmpId" runat="server" Text="Employee Id"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtEmpId" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox></td>

                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtEmpName" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblAttendDate" runat="server" Text="Attendance Date"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtAttenDate" runat="server" Text="0" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblServices" runat="server" Text="Services"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtServices" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblItaxRate2"  runat="server" Text="I.Tax Rate(Services)"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItaxRate2" Text="0" runat="server" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblItax" runat="server" Text="Income Tax(Services)"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItax" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblStax" runat="server" Text="Sales Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtStax" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNetSTax" runat="server" Text="Net Sales Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNetSTax" runat="server" Text="0" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblPST" runat="server" Text="PST" ToolTip="Punjab Services Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtPST" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBillAmount" runat="server" Text="Bill Amount"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtBillAmount" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblTotalTax" runat="server" Text="Total Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTotalTax" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                    </tr>
                    </table></div></div></div>
   
</asp:Content>
