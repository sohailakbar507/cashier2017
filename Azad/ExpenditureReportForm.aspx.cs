﻿using CashierModel;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ExpenditureReportForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Cls_Login login = (Cls_Login)Session["Login"];
            this.dtpFromDate.Text = login.yearstart.ToShortDateString();
            this.dtpToDate.Text = DateTime.Now.ToShortDateString();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Cls_Login login = (Cls_Login)Session["Login"];
            BudgetCMOEntities ent = new BudgetCMOEntities();
            var rec = ent.BudgetEXPENDITURE(Convert.ToDateTime(dtpFromDate.Text),Convert.ToDateTime(dtpToDate.Text),login.fyear);
            Response.Redirect("ViewSTIReports.aspx?fyear="+login.fyear+ "&reportid=mbudget");
        }
    }
}