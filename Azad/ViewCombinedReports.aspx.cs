﻿using CashierModel;
using Microsoft.Reporting.WebForms;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ViewCombinedReports : System.Web.UI.Page
    {
        public object ReportViewer1 { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadReport();
            }
        }
        private void LoadReport()
        {
            string cbno = Request["cbno"];
            string fyear = Request["fyear"];
            string report_path = "";
            StiSqlDatabase sqlDB = new StiSqlDatabase();
            string ConnectionString = ConfigurationManager.ConnectionStrings["Reports"].ConnectionString;

            this.StiWebViewerFx1.ShowParametersButton = false;
            StiReport rpt1 = new StiReport();
            report_path = Server.MapPath("~/CashierReports/Sanction.mrt");
            rpt1.Load(report_path);
            sqlDB = (StiSqlDatabase)rpt1.Dictionary.Databases[0];
            sqlDB.ConnectionString = ConnectionString;
            rpt1.Compile();
            rpt1["cbNo"] = cbno;
            rpt1["fyear"] = fyear;
            rpt1.Render();
            StiWebViewerFx1.Report = rpt1;

            this.StiWebViewerFx2.ShowParametersButton = false;
            StiReport rpt2 = new StiReport();
            report_path = Server.MapPath("~/CashierReports/BillForm.mrt");
            rpt2.Load(report_path);
            sqlDB = (StiSqlDatabase)rpt2.Dictionary.Databases[0];
            sqlDB.ConnectionString = ConnectionString;
            rpt2.Compile();
            rpt2["cbNo"] = cbno;
            rpt2["fyear"] = fyear;
            rpt2.Render();
            StiWebViewerFx2.Report = rpt2;

            this.StiWebViewerFx3.ShowParametersButton = false;
            StiReport rpt3 = new StiReport();
            report_path = Server.MapPath("~/CashierReports/ComputerSheet.mrt");
            rpt3.Load(report_path);
            sqlDB = (StiSqlDatabase)rpt3.Dictionary.Databases[0];
            sqlDB.ConnectionString = ConnectionString;
            rpt3.Compile();
            rpt3["cbNo"] = cbno;
            rpt3["fyear"] = fyear;
            rpt3.Render();
            StiWebViewerFx3.Report = rpt3;

          
        }
    }
}