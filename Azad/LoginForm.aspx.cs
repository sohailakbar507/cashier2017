﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class LoginForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //txtPass.Text = " ";
                //txtusername.Text = " ";
                //txtusername.Focus();
                //if (Session["LoginMessage"] != null)
                //    this.lblMessage.Text = Session["LoginMessage"].ToString();
                //else
                //    this.lblMessage.Text = "";
                BudgetCMOEntities ent = new BudgetCMOEntities();
                var years = (from a in ent.FinancialYears orderby a.fyearId descending select a).ToList();
                rcbyears.DataSource = years;
                rcbyears.DataTextField = "Financial_Year";
                rcbyears.DataValueField = "fyearid";
                rcbyears.SelectedIndex = 0;
                rcbyears.DataBind();
            }


        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Login();
        }

        protected void btnChangePasswordForm_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangePassword.aspx");
        }

        private void Login()
        {
            BudgetCMOEntities cm = new BudgetCMOEntities();
            string name = txtusername.Value;
            string pass = Cls_Encrypt.Encrypt(txtPass.Value);

            var CheckUser = from a in cm.Users where a.Name == name && a.Password == pass select a;

            if (CheckUser.Count() > 0)
            {
                var users = CheckUser.First();
                Cls_Login login = new Cls_Login();
                string finyear = rcbyears.Text;
                FinancialYear fyear = (from a in cm.FinancialYears where a.Financial_Year == finyear select a).First();
                login.Userid = Convert.ToInt32(users.ID);
                login.Username = users.Name;
                login.Groupid = Convert.ToInt32(users.Group_ID);
                login.IsUpdateForm = Convert.ToBoolean(users.UpdateForm);
                login.fyear = fyear.Financial_Year;
                login.fyearid = fyear.fyearId;
                login.yearstart = fyear.YearStart;
                login.yearend = fyear.YearEnd;
                Session["Login"] = login;
                if (login.Groupid == 8)
                {
                    Response.Redirect("UpdateTelephone.aspx");
                }
                else if(login.Groupid==9)
                {
                    Response.Redirect("BudgetReportForm.aspx");
                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }

               
                //Session["Login"] = login;
               
            }
           
        }
    }
}
