﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Base.Design;
using Stimulsoft.Report.Dictionary;
using CashierModel;
using System.Configuration;

namespace Azad
{
    public partial class ViewPayReport : System.Web.UI.Page
    {
        private string report_path;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!IsPostBack)
                {
                    this.LoadReport();
                }
            }
        }


            private void LoadReport()
        {
            BudgetCMOEntities dg = new BudgetCMOEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            string ConnectionString = ConfigurationManager.ConnectionStrings["Reports"].ConnectionString;
            StiSqlDatabase sqlDB = new StiSqlDatabase();
            StiReport rpt = new StiReport();
            StiWebViewerFx1.ShowParametersButton = false;
            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("../LoginPage.aspx");
                return;
            }
            if (rptid == "PayReport")
            {
                BudgetCMOEntities db = new BudgetCMOEntities();
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                db.BudgetAllocationPAY(cbno, fyear);
                report_path = Server.MapPath("~/CashierReports/PayBillFinal2017.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;

            }
            if (rptid == "PayBill")
            {
                BudgetCMOEntities db = new BudgetCMOEntities();
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                db.BudgetAllocationPAY(cbno, fyear);
                report_path = Server.MapPath("~/CashierReports/PayReportFinal2017.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;
            }
            if (access == true)
            {

                
                rpt.Render();
                StiWebViewerFx1.Report = rpt;

            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }
        }
    }
}
