﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class BillsFinalGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UploadFinalGrid();
            }
        }

        private void UploadFinalGrid()
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            var bill = (from a in tel.AGDataSums
                        from b in tel.v_AdminStaff
                        where a.offcode == b.EmployeeId && a.cbno == "E-05" && a.fyear == "2016-2017"
                        select new
                        {
                            a.ag_id,
                            b.EmployeeId,
                            b.PIFRA,
                            b.EmployeeName,
                            a.sum_amount }).ToList();
            DGVFinalBill.DataSource = bill;
            DGVFinalBill.DataBind();
        }

        protected void DGVFinalBill_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void DGVFinalBill_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void DGVFinalBill_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities bill = new BudgetCMOEntities();
            foreach (GridViewRow row in DGVFinalBill.Rows)
            {
                TextBox cb = (TextBox)row.FindControl("txtAmountUpdated");
                int sumid = Convert.ToInt32(DGVFinalBill.DataKeys[row.RowIndex].Value);
                AGDataSum ag = new AGDataSum();
                AGDataSum update = (from a in bill.AGDataSums where a.ag_id == sumid select a).First();
                update.sum_amount = Convert.ToDouble(cb.Text);
                //update.fyear = (Convert.ToInt32(DateTime.Now.Year) + "-" + Convert.ToInt32(DateTime.Now.Year + 1)).ToString();
                bill.SaveChanges();

            }
        }
        protected void LinkBtnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportsForm.aspx");
        }
    }
}