﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="PayForm.aspx.cs" Inherits="Azad.Pay.PayForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function FillBilling(f) {
        $("#ContentPlaceHolder1_officername").val(f);
    }
      
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    
        
            <div class="panel panel-default">

                        <div class="panel-heading">
                        <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                        </h5>
                        </div>
                        
                        <div class="panel-body">
            <div>



        <table align="center" width="100%">
        
        <tr>
            <td>
                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>    
            </td>
            <td>
                <telerik:RadDateTimePicker ID="RDDate" runat="server" CssClass="form-control">
                </telerik:RadDateTimePicker>
            </td>
            <td>
                <asp:Label ID="lblHead" runat="server" Text="Head"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DDLHead" runat="server" CssClass="form-control" OnSelectedIndexChanged="DDLHead_SelectedIndexChanged1">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtDescr" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td>
            <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
        </td>
        <td>
                <asp:TextBox ID="txtPayAmount" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
            <asp:Label ID="lblEmpId" runat="server" Text="Employee ID"></asp:Label>
        </td>
        <td>
                <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblOffCode" runat="server" Text="Office Code"></asp:Label>
            </td>
            <td>
                <div class="row">
                <div class="col-lg-6">
                <div class="input-group">
                     <%--<input type="text" runat="server" id="officername" disabled="disabled" class="form-control" placeholder="Offcode..." />--%>
                    <asp:TextBox ID="officername" runat="server" Width="150px"  CssClass="form-control"></asp:TextBox>
                    <span class="input-group-btn">
                    <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" data-keyboard="false" data-backdrop="static" style="cursor:pointer" type="button">Select Officer !</button>
                    </span>
                </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                </div></td>
            
        </tr></table>
        <table align="center" width="100%">
        <tr>
        <td colspan="4" align="center">
            <asp:Label ID="lblHeading" runat="server" Text="Particulars of Bill" 
                Font-Bold="True" Font-Names="Arial Black" Font-Size="Large" ForeColor="#000066"></asp:Label>
        </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbldobill" runat="server" Text="Date of Bill"></asp:Label>    
            </td>
            <td>
                <telerik:RadDateTimePicker ID="RDDateofBill" runat="server" CssClass="form-control">
                </telerik:RadDateTimePicker>
            </td>
            <td>
            <asp:Label ID="lblBillAmount" runat="server" Text="Bill Amount"></asp:Label>
        </td>
        <td>
                <asp:TextBox ID="txtBillAmount" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            <asp:Label ID="lblDeduction" runat="server" Text="Deduction"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDeduction" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
            <asp:Label ID="lblCbNo" runat="server" Text="CB No."></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCBNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td>
              <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr> 
        
        <tr>
        
            <td colspan="4" align="center">
              <br />    
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="85px" class="btn btn-primary btn-lg" 
                    onclick="btnSave_Click"/>
            </td>
        </tr>

    </table></div>
    </div>
    </div>
    <div class="dataTable_wrapper">

     <table align="center" width="100%">
       
        <tr>
            <td align = "center">
                <asp:GridView ID="DGVPayForm" runat="server" AllowPaging="True" CellPadding="3" GridLines="None" AutoGenerateColumns="False" 
                    onpageindexchanging="DGVPayForm_PageIndexChanging" PageSize="5" 
                    onrowcancelingedit="DGVPayForm_RowCancelingEdit" 
                    onrowupdating="DGVPayForm_RowUpdating" 
                    onrowediting="DGVPayForm_RowEditing" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellSpacing="1" Width="1132px" OnRowCommand="DGVPayForm_RowCommand" OnRowDeleting="DGVPayForm_RowDeleting" OnPageIndexChanged="DGVPayForm_PageIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="ExpID" Visible = "false">
                            <ItemTemplate>
                            <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("date","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Head">
                        <ItemTemplate>
                            <asp:Label ID="lblHead" runat="server" Text= '<%#Eval("head")%>'></asp:Label>
                        </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text= '<%#Eval("amount")%>'></asp:Label>
                        </ItemTemplate>
                            <HeaderStyle Font-Bold="True" Height="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bill Date">
                        <ItemTemplate>
                            <asp:Label ID="lblBillDate" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CB No">
                        <ItemTemplate>
                            <asp:Label ID="lblCBNo" runat="server" Text= '<%#Eval("cbno")%>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text= '<%#Eval("remarks")%>'></asp:Label>
                        </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnEdit" CommandName="edit" CommandArgument='<%#Eval("exp_id") %>' runat="server"><img src="../Images/edit-notes.png" border = "0" alt = "" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                         <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("exp_id") %>' OnClientClick="return confirm('are you sure to delete');">
                                <img src="../Images/DELBUTTON.png" border = "0" alt = "" />
                             </asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                       

                        


                        <asp:TemplateField HeaderText="Cancel">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkBtnCancel" CommandName = "Cancel" runat="server"> <img src="../Images/Cancel.png" border = "0" alt = ""/> </asp:LinkButton>
                         </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings FirstPageText=">>" LastPageText="<<" />
                    <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                </asp:GridView>        
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
        </tr>
    </table>
    </div>


    
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
    <div class="modal-content"> 
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;an>tton>
        <h4 class="modal-title" id="gridSystemModalLabel">Select Office</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <div class="form-group">
               
               <div class="dataTable_wrapper">
                                
                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Employee Name</th>
                                            <th>Designation</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <%
                                        
                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            var officers = (from a in ent.v_AdminStaff select a).ToList();
                                            foreach (var abc in officers)
                                            { %>
                                    <tr>
                                    <td><%=abc.EmployeeId%></td>
                                    <td><%=abc.EmployeeName%></td>
                                    <td><%=abc.Desg%></td>
                                    <td> <span class="input-group-btn">
        <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.EmployeeId %>)" type="button" data-dismiss="modal">Select!</button>
      </span></td>
                                    </tr>
                                    <%}
                                    %>
                                    </tbody>
                                    </table>

         </div>
         </div>
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
</div>
</div>
</div>
</div>>

</asp:Content>
