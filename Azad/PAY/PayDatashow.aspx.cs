﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CashierModel;

namespace CashierWeb.PAY
{
    public partial class PayDatashow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridPay();
            }

        }

        private void LoadGridPay()
        {
            BudgetCMOEntities bg = new BudgetCMOEntities();
            var pay = (from a in bg.expenses orderby a.exp_id descending
                       select new { a.exp_id, a.date, a.head, a.descr, a.amount, a.dobill, a.billamount, a.tax, a.cbno, a.remarks }).Take(5).ToList();
            Session["PayData"] = pay;
            DGVPayForm.DataSource = pay;
            DGVPayForm.DataBind();
                        
        }

        protected void DGVPayForm_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVPayForm.PageIndex = e.NewPageIndex;
            DGVPayForm.DataBind();
        }

        protected void DGVPayForm_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void DGVPayForm_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label SSid = (Label)DGVPayForm.Rows[e.RowIndex].FindControl("lblExpId");
            int ssno = Convert.ToInt32(SSid.Text);
            Response.Redirect("PayForm.aspx?catid=3 & exp_id=" + ssno);

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MainPage.aspx");
        }

        protected void DGVPayForm_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DGVPayForm.EditIndex = e.NewEditIndex;
        }

        
    }
}