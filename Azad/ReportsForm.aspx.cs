﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ReportsForm : System.Web.UI.Page
    {
        private const string VERIFICATION = "Utility Bills";
        private const string TELEPHONE_BILLS = "Telephone Bills";
        private const string BUDGET_ALLOCATION = "Budget Allocation";
        private const string SANCTION = "Sanction";
        private const string BILL_FORM = "Bill Form";
        private const string COMPUTER_SHEET = "Computer Sheet";
        private const string ALL_ABOVE = "All Above";
        private const string SANCTION_BY_SO = "Sanction by SO";
        private const string CHANGE_SHEET = "Change Sheet";
        private const string SPECIAL_ALLOWANCE = "Special Allowance";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                List<string> menuOptions = new List<string>();
                menuOptions.Add(VERIFICATION);
                menuOptions.Add(TELEPHONE_BILLS);
                menuOptions.Add(BUDGET_ALLOCATION);
                menuOptions.Add(SANCTION);
                menuOptions.Add(BILL_FORM);
                menuOptions.Add(COMPUTER_SHEET);
                menuOptions.Add(ALL_ABOVE);
                menuOptions.Add(SANCTION_BY_SO);
                menuOptions.Add(CHANGE_SHEET);
                menuOptions.Add(SPECIAL_ALLOWANCE);
                rblReportCategories.DataSource = menuOptions;
                rblReportCategories.DataBind();
                this.txtYear.Text = (DateTime.Now.Year).ToString() + "-" + (DateTime.Now.Year +1).ToString();
               
                using (BudgetCMOEntities db = new BudgetCMOEntities())
                {
                    string fyear = this.txtYear.Text;
                    var cbList = (from x in db.expenses
                                  where x.fyear == fyear
                                  select x.cbno).Distinct().ToList();

                    this.rcbCBNo.DataSource = cbList;
                    this.rcbCBNo.DataBind();

                }
            }
            else
                this.ProcessReports();
        }

        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RadioButton btn = (RadioButton) sender;


            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "Utility Bills":
                    Response.Redirect("ViewReports.aspx?reportid=UtilityBills&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Telephone Bills":
                    Response.Redirect("ViewReports.aspx?reportid=TELEPHONEBills&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Budget Allocation":
                    Response.Redirect("ViewReports.aspx?reportid=bujallocation&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Bill Form":
                    Response.Redirect("ViewReports.aspx?reportid=BillForm&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Sanction":
                    Response.Redirect("ViewReports.aspx?reportid=Sanction&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Computer Sheet":
                    Response.Redirect("ViewReports.aspx?reportid=ComputerSheet&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Change Sheet":
                    Response.Redirect("ViewReports.aspx?reportid=CBSheet&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                
            }

        }

        protected void ProcessReports()
        {
            if (string.IsNullOrEmpty(this.rblReportCategories.SelectedValue))
            {
                this.lblError.Text = "Please Select any Report";
                return;
            }

            if (string.IsNullOrEmpty(this.rcbCBNo.Text))
            {
                this.lblError.Text = "Please Enter CB #";
                return;
            }

            if (string.IsNullOrEmpty(this.txtYear.Text))
            {
                this.lblError.Text = "Please Enter Year";
                return;
            }
            BudgetCMOEntities dbcontext = new BudgetCMOEntities();

            string cb = this.rcbCBNo.Text;
            string year = this.txtYear.Text;

            switch (this.rblReportCategories.SelectedValue)
            {
                case BUDGET_ALLOCATION:
                    ObjectParameter opStatus = new ObjectParameter("status", "");
                    dbcontext.BudgetAllocationComplete(cb, year, opStatus);
                    this.lblError.Text = opStatus.Value.ToString();
                    break;
                default:
                    this.lblError.Text = "Report Not Implemented Yet";
                    break;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("UtilityBillsVerification.aspx");
        }
    }
}