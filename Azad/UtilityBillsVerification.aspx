﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UtilityBillsVerification.aspx.cs" Inherits="Azad.UtilityBillsVerification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        $(document).ready(function () {
            $('.form-control').keypress(function (e) {
                //console.log(e.keyCode);
                if (e.keyCode == 13) {
                    var $this = $(this),
                        index = $this.closest('td').index();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').focus();
                    //$this.closest('tr').next().find('td').eq(index).find('.form-control').val("");
                    e.preventDefault();
                }
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
   <br /><br />

    <div class="container" style="position:center">
  <div class="row">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                                                    
    <h3 class="panel-title">Utility Bills Correction</h3>

                        </div>
                        <div class="panel-body">
    
    
        <div class="row">
  <div class="col-lg-6">
   <%-- <asp:LinkButton ID="LinkBtnFinal" runat="server" OnClick="LinkBtnFinal_Click" Font-Bold="False" Font-Size="Medium" Font-Underline="True"><span class="label label-primary">Final List</span></asp:LinkButton>--%>
     <a href="BillsFinalGrid.aspx">
                            <div class="panel-footer">
                                <span class="pull-left">Final List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>       
                   
  </div>
            
  <div class="col-lg-6">
    
        <%--<asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkBtnback_Click"><span class="label label-primary">Back</span></asp:LinkButton>--%>
      <a href="StimualReportsForm.aspx">
                            <div class="panel-footer">
                                <span class="pull-left">Go Back</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-left"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
  </div>
</div>
                <asp:GridView ID="DGVBill" runat="server" CssClass="table" AutoGenerateColumns= "False" CellPadding="4" 
                    HorizontalAlign="Center" 
                    onrowediting="DGVBill_RowEditing" AllowPaging="false" PageSize="20" Width="100%" 
                    onpageindexchanging="DGVBill_PageIndexChanging" 
                    onrowupdating="DGVBill_RowUpdating" DataKeyNames="exp_id" 
                     GridLines="None" OnRowCommand="DGVBill_RowCommand" OnRowDeleting="DGVBill_RowDeleting">
                    
          <Columns>
              
                <asp:TemplateField HeaderText="Exp_Id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField HeaderText="EmpId">
              <ItemTemplate>
                 <asp:Label ID="lblEmpId" runat="server" Text='<%#Eval("EmployeeId") %>'></asp:Label>
              </ItemTemplate>
                        
              </asp:TemplateField>          
              <asp:TemplateField HeaderText="Personal No.">
              <ItemTemplate>
                  <asp:TextBox ID="txtPifra" CssClass="form-control" runat="server" Text='<%#Eval("PIFRA") %>'></asp:TextBox>
              
              </ItemTemplate>
                        
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Emp. Name">
                <ItemTemplate>
                   <asp:Label ID="lblEmpName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
                                         
                </ItemTemplate>
                    <ControlStyle Width="200px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                        
              <asp:TemplateField HeaderText="Amount">
              <ItemTemplate>
                 <asp:TextBox ID="txtAmountUpdated" CssClass="form-control" runat="server" Text='<%#Eval("amount") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="100px" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
              <asp:TemplateField HeaderText="CbNo">
              <ItemTemplate>
                 <asp:Label ID="lblCBno" runat="server" Text='<%#Eval("cbno") %>'></asp:Label>
              </ItemTemplate>
                  <ControlStyle Width="100px" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
              <asp:TemplateField HeaderText="DoBill">
              <ItemTemplate>
                 <asp:TextBox ID="txtdobill" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="100px" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
             
               <asp:TemplateField HeaderText="BillReference">
              <ItemTemplate>
                  <asp:TextBox ID="txtbillrefUpdated" CssClass="form-control" runat="server" Text='<%#Eval("bilrefence") %>'></asp:TextBox>
              
              </ItemTemplate>
                        
              </asp:TemplateField>
              
              <asp:TemplateField HeaderText="Remarks">
              <ItemTemplate>
                  <asp:TextBox ID="txtRemarksUpdated" runat="server" Text='<%#Eval("remarks") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="100px" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                       <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("exp_id") %>' OnClientClick="return confirm('are you sure to delete');">
                                                Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
              </Columns>
                </asp:GridView>
            
         <br /><br />

       
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save Changes"  
                   onclick="btnSave_Click" />
                   
     </div>
</div></div><%--</div>--%></div>

</asp:Content>
