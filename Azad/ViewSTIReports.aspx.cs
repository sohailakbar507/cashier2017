﻿using CashierModel;
using Microsoft.Reporting.WebForms;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ViewSTIReports : System.Web.UI.Page
    {
        public object ReportViewer1 { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadReport();
            }
        }
        private void LoadReport()
        { 
            BudgetCMOEntities dg = new BudgetCMOEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            string ConnectionString = ConfigurationManager.ConnectionStrings["Reports"].ConnectionString;
            StiSqlDatabase sqlDB = new StiSqlDatabase();
            StiReport rpt = new StiReport();
            StiWebViewer1.ShowParametersButton = false;
            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("../LoginPage.aspx");
                return;
            }
           
            if(rptid== "PayBudget")
            {
                string fyear = Request["fyear"];
                string cbno = Request["cbno"];
                report_path = Server.MapPath("~/CashierReports/MonthWise_PAY.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
                rpt["cbno"] = cbno;

            }
            if (rptid == "PayBudgetReport")
            {
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/PayBudgetReport.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
            }
            if (rptid == "CashierBudget")
            {
                string fyear = Request["fyear"];
              
                report_path = Server.MapPath("~/CashierReports/MonthWise_CASHIER.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
                
            }
            if (rptid == "CashierBudgetReport")
            {
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/CashierBudgetReport.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
            }
            if (rptid == "Both")
            {
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/MultiBillFrm.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
            }

            if (rptid=="mbp")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiBillFrm.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["username"] = loggedInUser.Username;

            }

            if (rptid == "MultiSanc")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiSanction.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["musername"] = loggedInUser.Username;

            }
            if (rptid == "MultiSancOne")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiSanctionOne.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["musername"] = loggedInUser.Username;

            }
            if (rptid == "MultiSancTwo")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiSanctionTwo.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["musername"] = loggedInUser.Username;

            }
            if (rptid == "MultiSancThree")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiSanctionThree.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["musername"] = loggedInUser.Username;

            }
            if (rptid == "MultiCompSheet")
            {
                string mdate = Request["mdate"];
                string date = new Cls_Encrypt().GetDate(Convert.ToDateTime(mdate));
                report_path = Server.MapPath("~/CashierReports/MultiComputerSheet.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdate"] = date;
                rpt["username"] = loggedInUser.Username;

            }
            if (rptid == "Verification")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/Verification.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;
            }
            if (rptid == "TELEPHONEBills")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/TelePhoneBillsPrint.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;

            }
           
            if (rptid == "Sanction")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/Sanction.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;
            }

            if (rptid == "BillForm")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/BillForm.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;
            }
            if (rptid == "ComputerSheet")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/ComputerSheet.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbNo"] = cbno;
                rpt["fyear"] = fyear;
                
            }
            if (rptid == "ChangeSheet")
            {
                string cbno = Request["cbno"];
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/ChangeSheetFinal.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["cbno"] = cbno;
                rpt["fyear"] = fyear;
            }
            if (rptid == "CBWISEDETAIL")
            {
               
                report_path = Server.MapPath("~/CashierReports/CBWISEDETAIL19.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["username"] = loggedInUser.Username;
                //rpt["Catid"] ="";

            }
            if (rptid == "MEDICALCBWISE")
            {

                report_path = Server.MapPath("~/CashierReports/MEDICALBILLSDETAIL19.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
               
            }
            if (rptid == "mbudget")
            {
                string fyear = Request["fyear"];
                report_path = Server.MapPath("~/CashierReports/BudgetExpenditure.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fyear"] = fyear;
            }

            if (access == true)
            {             
                rpt.Render(true);
                StiWebViewer1.Report = rpt;     

                //rpt.Print(false, 1);
            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }
        }
    }
}