﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Azad.Startup))]
namespace Azad
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
