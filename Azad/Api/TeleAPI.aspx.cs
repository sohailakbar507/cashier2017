﻿using CashierModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad.Api
{
    public partial class TeleAPI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int tele_id = 0;
                BudgetCMOEntities ent = new BudgetCMOEntities();
                tele response = new tele();
                string id = Request["ID"];
                Response.Headers["Content-type"] = "application/json";
                if (string.IsNullOrEmpty(Request["action"]))
                {
                    string r = JsonConvert.SerializeObject("No action defined");
                    Response.Write(r);
                    return;
                }

                if (string.IsNullOrEmpty(Request["ID"]))
                    tele_id = 0;
                else
                    int.TryParse(Request["ID"], out tele_id);

                switch (Request["action"])
                {
                    case "save":
                        if (tele_id == 0)
                        {
                            response = new tele();
                           // response.teleid = Convert.ToInt32(Request["Teleid"]);
                            response.phno = Request["Phno"];
                            response.offid = Convert.ToInt32(Request["OffId"]);
                            ent.teles.Add(response);
                            ent.SaveChanges();
                        }

                        else
                        {

                            response = (from a in ent.teles where a.teleid == tele_id select a).First();
                           // response.teleid = Convert.ToInt32(Request["Teleid"]);
                            response.phno = Request["Phno"];                           
                            response.offid = Convert.ToInt32(Request["OffId"]);
                            ent.SaveChanges();
                        }

                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                    case "delete":
                        response = (from a in ent.teles where a.teleid == tele_id select a).First();
                        ent.teles.Remove(response);
                        ent.SaveChanges();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;
                    case "get":
                        response = (from a in ent.teles where a.teleid == tele_id select a).First();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                }
            }
        }
    }
}