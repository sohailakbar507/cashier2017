﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HealthCareForm.aspx.cs" Inherits="Azad.HealthCareForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Styles/MainStyleSheet.css" rel="stylesheet" type="text/css" />
    <title> Patient Form </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link  href="styles/GridPaging.css" rel="stylesheet" />
    <!-- MetisMenu CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet" />
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Kendo -->
    <link href="Kendo/kendo.common.min.css" rel="stylesheet" />
    
<link href="Kendo/kendo.rtl.min.css" rel="stylesheet" />
    
<link href="Kendo/kendo.custom.css" rel="stylesheet" />
   
<link href="Kendo/kendo.default.mobile.min.css" rel="stylesheet" />

  <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
 <script src="Scripts/Kendo/jszip.min.js"></script>
   <script src="Scripts/Kendo/kendo.all.min.js"></script>
 
   <script src="Scripts/Kendo/console.js"></script>
  
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
        </div>
         <div class="panel panel-default">

        <div class="panel-heading">
            <h3 align="center">&nbsp;<asp:Label ID="lblTitle" runat="server" Text="Patient Entry Form"></asp:Label>
            </h3>
        </div>

        <div class="panel-body">
            <div>

                <table align="center" width="60%">
                  
                    <tr>
                        <td>
                            <asp:Label ID="lblPtId" runat="server" Text="Patient ID"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtPtId" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ForeColor="#990000"></asp:TextBox>
                        </td>
                       <td>
                            <asp:Label ID="lblVisitingDate" runat="server" Text="Visiting Date"></asp:Label></td>
                        <td>
                            <telerik:RadDateTimePicker ID="VisitingDate" runat="server" Width="300px" CssClass="form-control">
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPtName" runat="server" Text="Patient Name"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtPtName" runat="server" Width="300px"  CssClass="form-control"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblPtAddress"  runat="server" Text="Patient Address"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtPtAddress" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMob" runat="server" Text="Mobile #:"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txMobile" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblPtToken" runat="server" Text="Token #"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtToken" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                                      
                    <tr>
                        <td>
                            <asp:Label ID="lblHistory" runat="server" Text="Patient History"></asp:Label></td>
                        <td colspan="3">
                            <asp:TextBox ID="txtPtHistory" runat="server" Width="810px" Height="80px"
                                CssClass="form-control"></asp:TextBox>
                        </td></tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDiagnosis" runat="server" Text="Diagnosis"></asp:Label></td>
                        <td colspan="3">
                            <asp:TextBox ID="txtDiagnosis" runat="server" Width="810px" Height="80" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAdvised" runat="server" Text="Advised"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtAdvised" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblLastVisit" runat="server" Text="Last Visited On"></asp:Label></td>
                         <td>
                            <telerik:RadDateTimePicker ID="dtLastVisit" runat="server" Width="300px" CssClass="form-control">
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFee" runat="server" Text="Fee Charged"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtFee" CssClass="form-control" runat="server" Width="300px"></asp:TextBox></td>
                        </tr>
                    <tr>
                            <td><asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></td>
                        <td colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" Width="810px" Height="80" CssClass="form-control"></asp:TextBox>
                        </td>

                    </tr>               
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click1" class="btn btn-primary btn-lg" Width="85px"
                                Text="Save" />
                        </td>

                    </tr>
                     </table></div></div></div>
    </form>
</body>
</html>
