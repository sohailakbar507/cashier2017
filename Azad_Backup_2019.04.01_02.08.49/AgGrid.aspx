﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AgGrid.aspx.cs" Inherits="Azad.AgGrid" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3 align="center"> CB Sheet with Bill Amounts </h3>
    <asp:GridView ID="AGdataGridView" runat="server" AutoGenerateColumns="False" AllowPaging="true" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnPageIndexChanging="AGdataGridView_PageIndexChanging" PageSize="10" Width="444px" HorizontalAlign="Center" OnPageIndexChanged="AGdataGridView_PageIndexChanged">
        
        <AlternatingRowStyle BackColor="#F7F7F7" />

        <Columns>
                        <asp:TemplateField HeaderText="CB No.">
                        <ItemTemplate>
                            <asp:Label ID="lblcbno" runat="server" Text='<%#Eval("cbno") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Office Code">
                        <ItemTemplate>
                            <asp:Label ID="lblOffId" runat="server" Text='<%#Eval("offcode") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtSum_Amount" runat="server" Text='<%#Eval("Sum_amount") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                                           

                    </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    
    <table align="center" style="border:01px dashed gray;border-collapse:collapse;">
        <tr>
            <td align="center">

                <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save Changes" Width="151px" 
                   onclick="btnSave_Click" />
                   
                <asp:Button ID="btnCopytoExcel" class="btn btn-primary btn-lg" runat="server" 
                    Text="Copy to Excel" Width="150px" 
                   onclick="btnVerify_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
