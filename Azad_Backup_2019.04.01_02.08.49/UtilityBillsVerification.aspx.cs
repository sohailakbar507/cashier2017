﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class UtilityBillsVerification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UploadGrid();
            }

        }

        private void UploadGrid()
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            string cb = Request["cbno"];
            string fyear = Request["fyear"];
            var bill = (from a in tel.expenses
                        from b in tel.v_AdminStaff
                        where a.offcode == b.EmployeeId  && a.cbno == cb && a.fyear == fyear
                        orderby b.EmployeeName
                        select new
                        {
                            a.exp_id,
                            b.EmployeeId,
                            b.PIFRA,
                            a.cbno,
                            a.dobill,
                            a.fyear,
                            b.EmployeeName,
                            a.amount,
                            a.bilrefence,
                            a.remarks
                        }).ToList();
            DGVBill.DataSource = bill;
            DGVBill.DataBind();
        }

        protected void DGVBill_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void DGVBill_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void DGVBill_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities bill = new BudgetCMOEntities();
            foreach (GridViewRow row in DGVBill.Rows)
            {
                //TextBox pifra = (TextBox)row.FindControl("txtPifra");
                TextBox amount = (TextBox)row.FindControl("txtAmountUpdated");
                TextBox remarks = (TextBox)row.FindControl("txtRemarksUpdated");
                TextBox billref = (TextBox)row.FindControl("txtbillrefUpdated");
                TextBox dobill = (TextBox)row.FindControl("txtdobill");
                int sumid = Convert.ToInt32(DGVBill.DataKeys[row.RowIndex].Value);
                expense exp = new expense();
                expense update = (from a in bill.expenses where a.exp_id == sumid select a).First();
                update.amount = Convert.ToDouble(amount.Text);
                update.remarks = remarks.Text;
                update.bilrefence = billref.Text;
                if (!
                    string.IsNullOrEmpty(dobill.Text))
                {
                    update.dobill = Convert.ToDateTime(dobill.Text);
                }
                //update.fyear = (Convert.ToInt32(DateTime.Now.Year) + "-" + Convert.ToInt32(DateTime.Now.Year + 1)).ToString();
                bill.SaveChanges();
            }
        }

        protected void LinkBtnFinal_Click(object sender, EventArgs e)
        {
            Response.Redirect("BillsFinalGrid.aspx");
        }

        protected void LinkBtnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReportsForm.aspx");
        }

        protected void DGVBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {

                Cls_Login login = (Cls_Login)Session["login"];
                BudgetCMOEntities ent = new BudgetCMOEntities();
                CbDelete_Logs log = new CbDelete_Logs();
                int expid = Convert.ToInt32(e.CommandArgument);
                var logs = (from a in ent.expenses where a.exp_id == expid select a).First();
                log.cbno = logs.cbno;
                log.fyear = logs.fyear;
                log.amount = logs.amount;
                log.username = login.Username;
                log.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                ent.CbDelete_Logs.Add(log);
                ent.expenses.Remove(logs);
                ent.SaveChanges();
                UploadGrid();
            }
        }

        protected void DGVBill_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}