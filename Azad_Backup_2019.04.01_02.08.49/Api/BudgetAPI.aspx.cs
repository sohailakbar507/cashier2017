﻿using CashierModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad.Api
{
    public partial class BudgetAPI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int head_id = 0;
                BudgetCMOEntities ent = new BudgetCMOEntities();
                budget response = new budget();
                Response.Headers["Content-type"] = "application/json";
                if (string.IsNullOrEmpty(Request["action"]))
                {
                    string r = JsonConvert.SerializeObject("No action defined");
                    Response.Write(r);
                    return;
                }

                if (string.IsNullOrEmpty(Request["headid"]))
                    head_id = 0;
                else
                    int.TryParse(Request["headid"], out head_id);

                switch (Request["action"])
                {
                    case "save":
                        if (head_id == 0)
                        {
                            response = new budget();
                            response.headid = Convert.ToInt32(Request["headid"]);
                            response.head = Request["head"];
                            response.object1 = Request["object1"];
                            response.budget1 = Convert.ToDouble(Request["budget1"]);
                            response.expens = Convert.ToDouble(Request["expens"]);
                            response.supplygran = Convert.ToDouble(Request["supplygran"]);
                            ent.budgets.Add(response);
                            ent.SaveChanges();
                        }

                        else
                        {

                            response = (from a in ent.budgets where a.headid == head_id select a).First();
                            response.head = Request["head"];
                            response.object1 = Request["object1"];
                            response.budget1 = Convert.ToDouble(Request["budget1"]);
                            response.expens = Convert.ToDouble(Request["expens"]);
                            response.supplygran = Convert.ToDouble(Request["supplygran"]);
                            ent.SaveChanges();
                        }

                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                    case "delete":
                        response = (from a in ent.budgets where a.headid == head_id select a).First();
                        ent.budgets.Remove(response);
                        ent.SaveChanges();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;
                    case "get":
                        response = (from a in ent.budgets where a.headid == head_id  select a).First();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                }
            }


        }
    }
}