﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="BudgetReportForm.aspx.cs" Inherits="Azad.BudgetReportForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div style=" position:relative;left:200px; top:50px">
<table>
    <tr>
        <td>
            
        </td>
    </tr>
</table></div>
    <div style="width: 100%; height: 100%;">
    <div style="background-color: #f7f7f9; border: 1px solid #e1e1e8; width:100%; height: 700px; position:relative; left: 0px;top: 70px;">
        
        <div class="row">
        
    <div class="col-md-12" style="width:650px;">
                <div class="row">
            <div class="col-md-12">
                <h1> Budget Reports </h1>
            </div>
        </div>
               <div class="row">
                     <div class="col-md-12">
                         <asp:TextBox ID="txtYear"  runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-md-12">
                         <asp:TextBox ID="txtCbno"  runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblError" runat="server" or="Red"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RadioButtonList ID="rblReportCategories" runat="server" 
                            CssClass="myTable table table-hover" 
                            onselectedindexchanged="rblReportCategories_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            </div>
                </div>
            </div>
</asp:Content>
