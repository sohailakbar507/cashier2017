﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class BudgetReportForm : System.Web.UI.Page
    {
        private const string PayBudget = "PAY BUDGET";
        private const string PayBudgetMonthWise = "PAY BUDGET MONTHWISE";
        private const string CashierBudget = "CONTIGENCY BUDGET";
        private const string CashierBudgetMonthWise = "CONTIGENCY BUDGET MONTHWISE";
        private const string Both = "Both Reports";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<string> items = new List<string>();
                items.Add(PayBudget);
                items.Add(PayBudgetMonthWise);
                items.Add(CashierBudget);
                items.Add(CashierBudgetMonthWise);
                items.Add(Both);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                Cls_Login login = (Cls_Login)Session["Login"];
                txtYear.Text = login.fyear;
            }
        }

        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "PAY BUDGET":
                    Response.Redirect("ViewSTIReports.aspx?reportid=PayBudgetReport&fyear=" + txtYear.Text);
                    break;
                case "PAY BUDGET MONTHWISE":
                    Response.Redirect("ViewSTIReports.aspx?reportid=PayBudget&fyear=" + txtYear.Text + "&cbno=" + txtCbno.Text);
                    break;
              
                case "CONTIGENCY BUDGET":
                    Response.Redirect("ViewSTIReports.aspx?reportid=CashierBudgetReport&fyear=" + txtYear.Text);
                    break;
                case "CONTIGENCY BUDGET MONTHWISE":
                    Response.Redirect("ViewSTIReports.aspx?reportid=CashierBudget&fyear=" + txtYear.Text);
                    break;

                case "Both Report":
                    Response.Redirect("ViewSTIReports.aspx?reportid=Both&fyear=" + txtYear.Text);
                    break;
               
            }
        }
    }
}