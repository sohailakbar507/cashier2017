﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="VerifyForm.aspx.cs" Inherits="Azad.VerifyForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <br /><br />
    <div class="container">
  <div class="row">
   <div class="Absolute-Center is-Responsive">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                            <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="Verify Form" 
                            ForeColor="#000"></asp:Label>
                            </h5> 
                            
                        </div>
                        <div class="panel-body">
                        
        <table align="center" width="100%" style="border:1px dashed white;border-collapse:collapse;" cellpadding="2" cellspacing="1">

        <tr><td align="left">
            <asp:Label ID="Label1" runat="server" Text="NTC Lahore" Width="200px" 
                Height="50px" Font-Bold="true" Font-Size="X-Large"></asp:Label></td>
            <td align="center">
            <asp:Label ID="lblTotalNTC" runat="server" Width="200px" BackColor="#CCCCFF" 
                Font-Bold="True" Font-Size="XX-Large" BorderStyle="None" 
                    Height="50px"></asp:Label>

        </td></tr>
        <tr><td align="left">
            <asp:Label ID="Label7" runat="server" Text="Islamabad" Width="200px" 
                Height="50px" Font-Bold="true" Font-Size="X-Large"></asp:Label></td>
            <td align="center">
            <asp:Label ID="lblTotalIslamabad" runat="server" Width="200px" BackColor="#A8EAA8" 
                Font-Bold="True" Font-Size="XX-Large" BorderStyle="None" 
                    Height="50px"></asp:Label>

        </td></tr>
        <tr><td "left">
            <asp:Label ID="Label9" runat="server" Text="PTCL" Width="200px" 
                Font-Size="X-Large" Font-Bold="true"></asp:Label></td>
            <td align="center">
            <asp:Label ID="lblTotalPTCL" runat="server" Width="200px" BackColor="#FF9797" 
                Font-Bold="True" Font-Size="XX-Large" BorderColor="#CCCCCC" BorderStyle="None" 
                    Height="50px"></asp:Label>

        </td></tr> 

        
            <tr>
                <td>
                    <br /><br />
                </td>
            </tr>
            <tr>
            <td align="center" colspan="2">

                    <asp:Button ID="btnBack" class="btn btn-primary btn-lg" runat="server" 
                    Text="Back" Width="90px" onclick="btnBack_Click"/>
                    <br />
            <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblcbnos" runat="server" Text=" Apply CB Numbers:" BackColor="#000066" ForeColor="White"></asp:Label>

            </td>

        </tr>
        <tr>
            <td>
                <br />
                <asp:Label ID="lblCB" runat="server" Text="NTC Lahore" Font-Size="X-Large"></asp:Label></td>
                <td>
                <asp:TextBox ID="txtCB" runat="server"  CssClass="form-control"></asp:TextBox>  
                </td>
        </tr>

    <tr>
            <td>
                <asp:Label ID="lblCBIDB" runat="server" Text="NTC IDB" Font-Size="X-Large"></asp:Label></td>
                <td>
                <asp:TextBox ID="txtCBIDB" runat="server"  CssClass="form-control"></asp:TextBox>  
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPTCL" runat="server" Text="PTCL Lahore #" Font-Size="X-Large"></asp:Label></td>
                <td>
                <asp:TextBox ID="txtPTCL" runat="server"  CssClass="form-control"></asp:TextBox>  
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">

                    <br />

                    <asp:Button ID="btnUpdate" class="btn btn-primary btn-lg" runat="server" 
                    Text="Update" Width="90px" onclick="btnUpdate_Click" />
                    <asp:Button ID="btnPrint" class="btn btn-primary btn-lg" runat="server" 
                    Text="Report" Width="90px" onclick="btnPrint_Click" />
            <br />
            </td>
        </tr>
       </table>
       </div></div></div></div></div>
</asp:Content>
