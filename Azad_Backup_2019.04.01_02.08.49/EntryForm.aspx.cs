﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Input;

namespace Azad
{
    public partial class EntryForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    BudgetCMOEntities CMO = new BudgetCMOEntities();

                    int catid = Convert.ToInt32(Request.QueryString["catid"]);
                    if (catid == 3)
                    {
                        Response.Redirect("Pay/PayForm.aspx?catid=" + catid);
                    }
                    if (catid == 4)
                    {
                        Response.Redirect("~/Telephone.aspx");
                    }
                    var formtype = (from a in CMO.BudgetCategories where a.ID == catid select a).First();
                    lblTitle.Text = formtype.Cat_Name.ToString() + " Form";
                    var heads = (from a in CMO.Sub_BudgetCategories where a.CatID == catid select a).ToList();
                    DDLHead.DataSource = heads;
                    DDLHead.DataTextField = "SubCatName";
                    DDLHead.DataValueField = "SubCatCode";
                    DDLHead.DataBind();
                    //getOfficers();
                   // getParties();
                    LoadList();
                    //LoadList();
                }
                else
                {
                    // Response.Redirect("~/LoginForm.aspx");
                }
            }

        }
        private void EditRecord(int id)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            int expid = Convert.ToInt32(id);
            var abc = (from a in cmo.expenses
                       where a.exp_id == expid
                       select new { a.exp_id, a.head, a.unit, a.amount, a.taxrate, a.itax, a.stax, a.tax, a.diff, a.offcode, a.partycode, a.billno, a.dobill, a.cbno, a.Sanc_id, a.dosubtoag }).Take(5).ToList();
            if (abc.Count() > 0)
            {
                var x = abc.First();
                DDLHead.SelectedIndex = DDLHead.Items.IndexOf(DDLHead.Items.FindByValue(x.head.ToString()));
                txtAmount.Text = x.amount.ToString();
                txtItax.Text = x.itax.ToString();
                txtStax.Text = x.stax.ToString();
                officername.Text = x.offcode.ToString();
                //DDLParty.SelectedIndex = DDLParty.Items.IndexOf(DDLParty.Items.FindByValue(x.partycode.ToString()));
                txtpartycode.Text = x.partycode.ToString();
                if (x.billno != "")
                {
                    txtBillNo.Text = Convert.ToInt32(x.billno).ToString();
                }

               
                BillDate.DateInput.DisplayText = x.dobill.ToString();
                txtCBNo.Text = x.cbno.ToString();
            }
            }
        private void LoadList()
        {
            int catid = Convert.ToInt32(Request.QueryString["catid"]);
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            Cls_Login login = (Cls_Login)Session["login"];
            string headcode = DDLHead.SelectedValue;
            var abc = (from a in cmo.expenses
                       where a.head == headcode && a.fyear==login.fyear
                       orderby a.exp_id descending
                       select new { a.exp_id, a.head, a.unit,a.billamount, a.amount, a.taxrate, a.itax, a.stax, a.tax, a.diff, a.offcode, a.partycode, a.billno, a.dobill, a.cbno, a.Sanc_id, a.dosubtoag }).ToList();

            GVListShow.DataSource = abc;
            GVListShow.DataBind();
            Session["dtData"] = abc;

        }

        private void getOfficers()
        {

            BudgetCMOEntities CMO = new BudgetCMOEntities();
            //v_AdminStaff vtable = new v_AdminStaff();
            //var numb = int.Parse(vtable.BPS);
            var officer = (from a in CMO.v_AdminStaff orderby a.EmployeeId select a).ToList();

        }

        protected void DDLHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadList();
        }

        expense DataMapping(expense ef)
        {
            //expense ef = new expense();
            return ef;
        }

        protected void GVListShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void GVListShow_RowCancelingEdit1(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void GVListShow_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVListShow.EditIndex = e.NewEditIndex;
        }

        protected void GVListShow_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {            
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            if (Session["edit"] == null)
            {
                Cls_Login login = (Cls_Login)Session["Login"];
                if (Convert.ToDateTime(DateTime.Now.ToShortDateString())>=login.yearstart && Convert.ToDateTime(DateTime.Now.ToShortDateString())<=login.yearend)
                {                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Date is invalid please check date and try again.');", true);
                    return;
                }
                
               // int result = DateTime.Compare(Convert.ToDateTime(DateTime.Now.ToShortDateString()), login.yearstart);
               // int result1 = DateTime.Compare(Convert.ToDateTime(DateTime.Now.ToShortDateString()), login.yearend);
                expense ef = new expense();
                ef.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                ef.head = DDLHead.SelectedItem.Value;
                ef.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtAmount.Text);
                ef.taxrate = txtItaxRate.Text == string.Empty ? 0 : Convert.ToDouble(txtItaxRate.Text);
                ef.taxrate10 = txtItaxRate2.Text == string.Empty ? 0 : Convert.ToDouble(txtItaxRate2.Text);
                ef.itax10 = txtItax.Text == string.Empty ? 0 : Convert.ToDouble(txtItax.Text);
                ef.itax = txtItax1.Text == string.Empty ? 0 : Convert.ToDouble(txtItax1.Text);
                ef.amnt = txtPST.Text == string.Empty ? 0 : Convert.ToDouble(txtPST.Text);
                ef.services = txtServices.Text == string.Empty ? 0 : Convert.ToDouble(txtServices.Text);
                ef.stax = txtStax.Text == string.Empty ? 0 : Convert.ToDouble(txtStax.Text);
                ef.tax = txtTotalTax.Text == string.Empty ? 0 : Convert.ToDouble(txtTotalTax.Text);
                ef.diff = txtNetAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtNetAmount.Text);
                ef.billamount = txtBillAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtBillAmount.Text);
                ef.offcode = officername.Text == string.Empty ? 0 : Convert.ToInt32(officername.Text);
                //ef.partycode = Convert.ToInt32(DDLParty.SelectedItem.Value);
                ef.partycode = txtpartycode.Text == string.Empty ? 0 : Convert.ToInt32(txtpartycode.Text);
                ef.billno = txtBillNo.Text;
                ef.dobill = BillDate.SelectedDate;
                ef.cbno = txtCBNo.Text;
                ef.amtinwords = txtAmountinWords.Text;
                ef.dosubtoag = dosubtoag.SelectedDate;
                ef.fyear = login.fyear;
                ef.UserName = login.Username;
                cmo.expenses.Add(ef);
                cmo.SaveChanges();
                LoadList();
            }
            else
            {
                int cashid = Convert.ToInt32(Session["pram"]);

                expense E = new expense();
                E = (from a in cmo.expenses where a.exp_id == cashid select a).First();
                E.date = DateTime.Now;
                E.head = DDLHead.SelectedItem.Value;
                E.amnt = txtPST.Text == string.Empty ? 0 : Convert.ToDouble(txtPST.Text);
                E.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtAmount.Text);
                E.taxrate = txtItaxRate.Text == string.Empty ? 0 : Convert.ToDouble(txtItaxRate.Text);
                E.itax = txtItax.Text == string.Empty ? 0 : Convert.ToDouble(txtItax.Text);
                E.services = txtServices.Text == string.Empty ? 0 : Convert.ToDouble(txtServices.Text);
                E.stax = txtStax.Text == string.Empty ? 0 : Convert.ToDouble(txtStax.Text);
                E.tax = txtTotalTax.Text == string.Empty ? 0 : Convert.ToDouble(txtTotalTax.Text);
                E.diff = txtNetAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtNetAmount.Text);
                E.billamount = txtBillAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtBillAmount.Text);
                E.offcode = Convert.ToInt32(officername.Text);
                E.partycode = Convert.ToInt32(txtpartycode.Text);
                E.billno = txtBillNo.Text;
                E.dobill = BillDate.SelectedDate;
                E.cbno = txtCBNo.Text;
                E.dosubtoag = dosubtoag.SelectedDate;
                E.amtinwords = txtAmountinWords.Text;
                cmo.SaveChanges();
                Session["edit"] = null;
                Session["pram"] = null;
                LoadList();
            }
        }

        protected void GVListShow_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                // Label SSid = (Label)GVListShow.Rows[e.RowIndex].FindControl("lblExpId");
                EditRecord(Convert.ToInt32(e.CommandArgument));
                Session["edit"] = 1;
                Session["pram"] = e.CommandArgument;
            }

            if (e.CommandName == "Delete")
            {
                
                Cls_Login login = (Cls_Login)Session["login"];
                BudgetCMOEntities ent = new BudgetCMOEntities();
                CbDelete_Logs log = new CbDelete_Logs();
                int expid = Convert.ToInt32(e.CommandArgument);
                var logs = (from a in ent.expenses where a.exp_id == expid select a).First();
                log.cbno = logs.cbno;
                log.fyear = logs.fyear;
                log.amount = logs.amount;
                log.username = login.Username;
                log.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                ent.CbDelete_Logs.Add(log);
                ent.expenses.Remove(logs);
                var delag = (from a in ent.ags where a.cbno == logs.cbno && a.fyear == login.fyear select a).ToList();
                if(delag.Count() > 0)
                {
                    var agdelete = delag.First();
                    ent.ags.Remove(agdelete);
                }
                ent.SaveChanges();
                LoadList();
            }

            if (e.CommandName == "Cancel")
            {
                Response.Redirect("DashBoard.aspx");
            }
        }

        protected void GVListShow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVListShow.PageIndex = e.NewPageIndex;
            GVListShow.DataSource = Session["dtData"];
            GVListShow.DataBind();
        }

        protected void GVListShow_PageIndexChanged(object sender, EventArgs e)
        {
        }
        protected void GVListShow_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }
    }
}
