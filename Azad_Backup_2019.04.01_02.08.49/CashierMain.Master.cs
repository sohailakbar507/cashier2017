﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace Azad
{
    public class MenuOption
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
    public partial class CashierMain : System.Web.UI.MasterPage
    {
        public List<MenuOption> menu;
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    BudgetCMOEntities ent = new BudgetCMOEntities();
                    Cls_Login login = (Cls_Login)Session["login"];
                    this.menu = (from a in ent.MenuAccessRights
                                 from b in ent.menuRights
                                 where a.GroupId == login.Groupid && a.M_ID == b.menu_id && b.IsReport == false
                                 select new MenuOption { Name = b.MenuName, Url = b.Url }).ToList();

                }
                else
                {
                    Response.Redirect("~/LoginForm.aspx");
                }
            }


        }

        public void Logout()
        {
            Session["login"] = null;
            Response.Redirect("~/LoginForm.aspx");
        }

        protected void btnLogout_Click1(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("LoginForm.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Logout();
        }
    }

}