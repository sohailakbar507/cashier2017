﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="UpdateTelephone.aspx.cs" Inherits="Azad.UpdateTelephone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <%-- kendo.common.min.css contains common CSS rules used by all Kendo themes --%>
   
   <%-- <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>--%>
 <script type="text/javascript">
    var api_url = "/Api/TeleAPI.aspx";
    var tele_id = 0;
     function Tele_btnAdd_OnClick() {
         teleid_id = 0;
        $("#txtphno").val("");
        $("#txtoffid").val("");
    }

    function Tele_btnSaveChanges_OnClick() {
        var data = {
            action: "save",
            ID: tele_id,
            Phno: $("#txtphno").val(),
            OffId: $("#txtoffid").val()
        };
        $.get(api_url, data, function (response) {
            var r = JSON.parse(response);
            
                location.reload();
            
        })
    }

    function Tele_btnDelete_OnClick(id, phno) {
        
        if (confirm("Are you sure to delete " + phno + "?")) {
            $.get(api_url, { action: "delete", ID: id }, function (response) {
                var r = JSON.parse(response);
                
                    location.reload();
                
            });
        }
    }

     function Tele_btnEdit_OnClick(id) {
         $.get(api_url, { action: "get", ID: id }, function (response) {
             var r = JSON.parse(response);
             $("#txtphno").val(r.phno);
             $("#txtoffid").val(r.offid);
             tele_id = id;
         });
     }
     
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="white-box" style="width: 800px" >
            <div class="table-responsive">
     <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" onclick="Tele_btnAdd_OnClick()" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info">Add New</button>
                        </div>
                        <div class="panel-body">
                             <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>Phone Number</th>
                                            <th>Officer Id</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%

                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            var tele = (from a in ent.teles select a).ToList();
                                            foreach (var abc in tele)
                                            { %>
                                        <tr>
                                            <td><%=abc.phno%></td>
                                            <td><%=abc.offid%></td>
                                            <td>
                                           
                                                <button type="button" onclick="Tele_btnEdit_OnClick(<%=abc.teleid%>)" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info btn-circle center-block"><i class="fa fa-edit"></i></button>
                                            </td>
                                            <td>
                                                <button type="button" onclick="Tele_btnDelete_OnClick(<%=abc.teleid %>,'<%=abc.phno %>')" class="btn btn-danger btn-circle center-block"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="panel-footer">
                            Panel Footer
                        </div>
                    </div>
                </div></div>
    
    <div class="col-md-4" style="z-index:9999;">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="white-box" style="width: 400px" >
                        <div class="table-responsive">
                    <div class="modal-content">
                        <div class="modal-header">
                       
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel">CM Office Telephone Information</h4>
                        </div>
                        <div class="modal-body">
                                 
                               
                                <div class="form-group m-b-40">
                                    <label for="txtName">Phone Number</label>
                                    <input type="text"  class="form-control text-left" id="txtphno" name="txtphno" required/><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                                <div class="form-group m-b-40">
                                     <label for="txtAddress">Office Id</label>
                                    <input type="text" class="form-control text-left" id="txtoffid" name="txtoffid" required/><span class="highlight"></span> <span class="bar"></span>
                                   
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSaveChanges" class="btn btn-success waves-effect waves-light m-r-10" onclick="Tele_btnSaveChanges_OnClick()">Save Changes</button>
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                   
                </div>
               
            </div>
            </div></div></div>

<%--    <script type="text/javascript">
           $(document).ready(function () {

     var table= $('#Contats-Table').DataTable({
                   "columnDefs": [
                       
                       { "width": "5%", "targets": 1 },
                       { "width": "5%", "targets": 5 },
                       
                      
                    
                       
  ]});
    
</script>--%>
</asp:Content>
