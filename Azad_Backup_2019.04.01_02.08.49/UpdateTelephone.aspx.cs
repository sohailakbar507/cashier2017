﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CashierModel;

namespace Azad
{
    public partial class UpdateTelephone : System.Web.UI.Page
    {
        public List<tele> telelist;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BudgetCMOEntities ent = new BudgetCMOEntities();
                telelist = (from a in ent.teles select a).ToList();

            }
        }
    }
}