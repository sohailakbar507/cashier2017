﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BudgetMaster.Master" AutoEventWireup="true" CodeBehind="BudgetForm.aspx.cs" Inherits="Azad.BudgetForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.form-control').keypress(function (e) {
                //console.log(e.keyCode);
                if (e.keyCode == 13) {
                    var $this = $(this),
                        index = $this.closest('td').index();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').focus();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').val("");
                    e.preventDefault();
                }
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
</telerik:RadScriptManager>
    <br /><br />
    <div class="container" style="position:center">
  <div class="row">
   <%--<div class="Absolute-Center is-Responsive">--%>
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                 <div class="row">
  <div class="col-lg-6">                                   
    <h3 class="panel-title">Budget Entry Form</h3>
      </div>
                     <div class="col-lg-6"> 
                             <a href="EntryForm.aspx?catid=1">
                            <div class="panel-footer" style="background-color:#337ab7;border-top:0px;">
                                <span class="pull-right" style="color:#fff;">Go Back</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-left" style="color:#fff;"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                         </div>
                        </div>
                            </div>
                        <div class="panel-body">
    <table align="center" style="border:0px dashed gray;border-collapse:collapse;" width="100%">
    <tr><td>
        <div class="modal-header">
    Enter Finanical Year :
    
        <div class="row">
  <div class="col-lg-6">
    <div class="input-group">
      <asp:TextBox ID="txtYear" CssClass="form-control" runat="server"></asp:TextBox>
      <span class="input-group-btn">
    
        <asp:Button ID="ViewDetail" runat="server" Text="View" class="btn btn-primary btn-small" Width="100px" OnClick="ViewDetail_Click" />
        
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div>
            </div>
     </td>
        <td>
            
        </td>
     </tr>
  
            <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="DGVTelephone" runat="server" CssClass="table" AutoGenerateColumns= "False" CellPadding="4" 
                    HorizontalAlign="Center" 
                    onrowediting="DGVTelephone_RowEditing" PageSize="20" Width="100%" 
                    onpageindexchanging="DGVTelephone_PageIndexChanging" 
                    onrowupdating="DGVTelephone_RowUpdating" DataKeyNames="headid" 
                     GridLines="None">
                    
          <Columns>
                <asp:TemplateField HeaderText="headid" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblHeadid"  runat="server" Text='<%#Eval("headid") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Head">
                        <ItemTemplate>
                            <asp:Label ID="lblHead" runat="server"  Text='<%#Eval("head") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Major Head">
                        <ItemTemplate>
                            <asp:Label ID="lblMajorHead" runat="server" Text='<%#Eval("majorhead") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                     <asp:TemplateField HeaderText="Object">
                        <ItemTemplate>
                            <asp:Label ID="lblObject" runat="server" Text='<%#Eval("object") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Budget">
                        <ItemTemplate>
                              <asp:TextBox ID="txtbudget" CssClass="form-control" runat="server" Text='<%#Eval("budget1") %>'></asp:TextBox>
                         </ItemTemplate>
                        
                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Supplymentry Grant">
                        <ItemTemplate>
                                <asp:TextBox ID="txtgrant" runat="server" CssClass="form-control" Text='<%#Eval("supplygran") %>'></asp:TextBox>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expense">
                        <ItemTemplate>
                            
                        <asp:TextBox ID="txtexpense" runat="server" CssClass="form-control" Text='<%#Eval("expens") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
       
        </table>
         <br /><br />

       
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save Changes" 
                   onclick="btnSave_Click" />
                   
     </div>
</div></div><%--</div>--%></div>
    
</asp:Content>
