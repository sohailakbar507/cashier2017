﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Change_Click(object sender, EventArgs e)
        {
           

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities cm = new BudgetCMOEntities();
            string username = txtusername.Value;
            string oldpass = Cls_Encrypt.Encrypt(txtPass.Value);
            string newpass = Cls_Encrypt.Encrypt(txtNewPassword.Value);
            var chkUser = from a in cm.Users where a.Name == username && a.Password == oldpass select a;

            if (chkUser.Count() > 0)
            {
                var users = chkUser.First();
                users.Password = newpass;
                cm.SaveChanges();
                Response.Redirect("loginForm.aspx");
            }
            else
            {
                //Messagebox.show("Incorrect Information");
            }
        }
    }
}