﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class BudgetMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    BudgetCMOEntities ent = new BudgetCMOEntities();
                    Cls_Login login = (Cls_Login)Session["login"];
                }
                else
                {
                    Response.Redirect("~/LoginForm.aspx");
                }
            }
        }
    }
}