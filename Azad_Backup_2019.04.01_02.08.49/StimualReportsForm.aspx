﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="StimualReportsForm.aspx.cs" Inherits="Azad.StimualReportsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .myTable > tbody > tr > td > label
    {
        font-weight:normal;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    
    <div style=" position:relative;left:200px; top:50px">
<table>
    <tr>
        <td>
            <h1> Cashier Reports </h1>
        </td>
    </tr>
    <tr>
        <td align="right">
        <%--<asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Correction Form</asp:LinkButton>--%>
        </td>
    </tr>
</table></div>
<div style="width: 100%; height: 100%;">
    <div style="background-color: #f7f7f9; border: 1px solid #e1e1e8; width:100%; height: 700px; position:relative; left: 0px;top: 70px;">
        
        <div class="row">
            <div class="col-md-6" style="border-right: 1px solid #e1e1e8; height: 700px; width:350px;">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Report
                    <span class="glyphicon glyphicon-chevron-down"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                          <li><a href="MultiReportsForm.aspx?reportid=mbp">Multiple BillForm</a></li>
                          <li><a href="MultiReportsForm.aspx?reportid=MultiSanc">Multiple Sanction</a></li>
                             <li><a href="MultiReportsForm.aspx?reportid=MultiSancOne">Multiple Sanction I</a></li>
                             <li><a href="MultiReportsForm.aspx?reportid=MultiSancTwo">Multiple Sanction II</a></li>
                        <li><a href="MultiReportsForm.aspx?reportid=MultiSancThree">Multiple Sanction III</a></li>
                          <li><a href="MultiReportsForm.aspx?reportid=MultiCompSheet">Multiple Computer Sheet</a></li>
                          <li><a href="ExpenditureReportForm.aspx">Expenditure Report</a></li>
                          <li><a href="ViewSTIReports.aspx?reportid=CBWISEDETAIL">HEAD/CB WISE Report</a></li>
                          <li><a href="ViewSTIReports.aspx?reportid=MEDICALCBWISE">MEDICAL CB WISE Report</a></li>
                           <%--<li><a href="MultiReportsForm.aspx?reportid=MultiCompSheet">CB Wise Report</a></li>--%>
        <%--                  <%if (!this.IsPostBack)
                            {
                                if (Session["login"] != null)
                                {
                                     if (Session["cb"] != null)
                        {
                            rcbCBNo.Text = Session["cb"].ToString();
                        }

                                    CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                    CashierModel.Cls_Login login = (CashierModel.Cls_Login)Session["login"];
                                    var checkReportsRights = from a in ent.MenuAccessRights where a.GroupId == login.Groupid select a;
                                    if (checkReportsRights.Count() > 0)
                                    {
                                        
                                        foreach (var m in checkReportsRights)
                                        {
                                            var menuname1 = (from a in ent.menuRights where a.menu_id == m.M_ID && a.IsReport == true select a);
                                            if (menuname1.Count() > 0)
                                            {
                                                var menuname = menuname1.First();
                                            
                                            %>
                                             <li><a href="<%=this.ResolveUrl(menuname.Url) %>"><%=menuname.MenuName%></a></li>
                                                 <%
                                            }
                                        }
                                        
                                    }                                    

                                }
                                else
                                {
                                    Response.Redirect("~/LoginForm.aspx");
                                }
                            } %> --%>
                            
                    </ul>
                    
                </div>
            </div>
            
            <div class="col-md-6" style="width:450px;">
                <div class="row">
                    <div class="col-md-12">
                    Reference/CB #
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        
                        <telerik:RadComboBox ID="rcbCBNo" Width="250" Runat="server" Filter="StartsWith" ClientIDMode="AutoID" CssClass="form-control">
                        </telerik:RadComboBox>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-12">
                         <asp:TextBox ID="txtYear" Width="250" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblError" runat="server" or="Red"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RadioButtonList ID="rblReportCategories" runat="server" 
                            CssClass="myTable table table-hover" 
                            onselectedindexchanged="rblReportCategories_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
