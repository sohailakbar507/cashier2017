﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class DashBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    BudgetCMOEntities ent = new BudgetCMOEntities();
                    Cls_Login login = (Cls_Login)Session["login"];
                    if (login.Groupid == 7)
                    {
                        Response.Redirect("UtilityBills.aspx");

                    }
                    var getuseraccess = (from a in ent.UsersHeads
                                             from b in ent.BudgetCategories
                                             where a.groupId == login.Groupid && a.headId == b.ID
                                             select new { b.Cat_Name, a.headId, b.Code }).ToList();
                        grdViewAlbum.DataSource = getuseraccess;
                        grdViewAlbum.DataBind();
                    }
                
                else
                {
                    Response.Redirect("~/LoginForm.aspx");
                }
            }

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session["login"] = null;
            Response.Redirect("~/LoginForm.aspx");
        }
    }

}

