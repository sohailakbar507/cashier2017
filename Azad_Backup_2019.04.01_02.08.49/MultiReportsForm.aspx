﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="MultiReportsForm.aspx.cs" Inherits="Azad.MultiReportsForm" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager runat="server"></asp:ScriptManager>
    
    <div class="container" style="position:center">
  <div class="row">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                                                    
    <h3 class="panel-title" align="center"> Multi Reports </h3>

                        </div>
                        <div class="panel-body">
     <div class="row" align="center">
        <telerik:RadDateTimePicker ID="MultiDatePicker" runat="server"></telerik:RadDateTimePicker>
         <br />
        <br /></div>
    
    <div class="dataTable_wrapper">
    <asp:GridView ID="DGVMultiBudget" runat="server" HorizontalAlign="Center" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Height="221px" Width="726px">
        <Columns>

            <asp:TemplateField HeaderText="Exp_Id" Visible="false">
             <ItemTemplate>
                <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
             </ItemTemplate>
             </asp:TemplateField>
             
            <asp:TemplateField HeaderText="Date">
                 <ItemTemplate>
                     <asp:Label ID="lbldate" runat="server" Text='<%#Eval("date","{0:dd/MM/yyyy}") %>'></asp:Label>
                  </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Head">
                <ItemTemplate>
                    <asp:Label ID="lblhead" runat="server" Text='<%#Eval("head")%>'></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="CBNo">

                <ItemTemplate>
                    <asp:Label ID="lblcbno" runat="server" Text='<%#Eval("cbno")%>'></asp:Label>
                 </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Amount">
                <ItemTemplate>
                    <asp:Label ID="lblamount" runat="server" Text='<%#Eval("amount")%>'></asp:Label>
            </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fyear">
                 
                <ItemTemplate>
                    <asp:Label ID="lblfyear" runat="server" Text='<%#Eval("fyear")%>'></asp:Label>
            </ItemTemplate>

            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>
        <br />
  </div>

     <div class="row" align="center">

    <asp:LinkButton ID="lnkShowData" class="btn btn-primary btn-lg" runat="server" OnClick="lnkShowData_Click">Show Data</asp:LinkButton>  
    <asp:LinkButton ID="LinkPrint" class="btn btn-primary btn-lg" runat="server" OnClick="LinkPrint_Click">Print Preview</asp:LinkButton>
    </div>
    </div></div></div></div>
</asp:Content>
