﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class ShowForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    GetHead();
                    LoadGrid();
                }
                else
                {
                    Response.Redirect("~/LoginForm.aspx");
                }
            }

        }

        private void LoadGrid()
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            int cashid = Convert.ToInt32(Request["catid"]);
            var abc = (from a in cmo.expenses
                       orderby a.exp_id descending
                       select new { a.exp_id, a.head, a.unit, a.amount, a.taxrate, a.itax, a.stax, a.tax, a.diff, a.offcode, a.partycode, a.billno, a.dobill, a.cbno, a.Sanc_id }).Take(1).ToList();

            GVCashier.DataSource = abc;
            GVCashier.DataBind();
            Session["CashierData"] = abc;

        }

        private void GetHead()
        {

        }

        protected void DDLHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            BudgetCMOEntities cm = new BudgetCMOEntities();
            string head = (DDLHead.SelectedItem.Text);
            var abc = from a in cm.expenses
                      where a.head == head
                      select new { a.head, a.unit, a.amount, a.taxrate, a.itax, a.stax, a.tax, a.diff, a.offcode, a.partycode, a.billno, a.dobill, a.cbno, a.Sanc_id };

            if (abc.Count() > 0)
            {
                GVCashier.DataSource = abc;
                GVCashier.DataBind();
            }
        }

        protected void GVCashier_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVCashier.PageIndex = e.NewPageIndex;
            GVCashier.DataBind();
        }
    }

}

