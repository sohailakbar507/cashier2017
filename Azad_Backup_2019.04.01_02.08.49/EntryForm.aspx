﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="EntryForm.aspx.cs" Inherits="Azad.EntryForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function FillBilling(f) {
            $("#ContentPlaceHolder1_officername").val(f);
        }

        function SelectParty(f) {
            $("#ContentPlaceHolder1_txtpartycode").val(f);
        }
        function OnFiguresChange() {
            var obj1 = document.getElementById('<%=txtAmount.ClientID %>');
            var obj2 = document.getElementById('<%=txtAmountinWords.ClientID %>');
                console.log("obj1=" + obj1);
                console.log("obj2=" + obj2);
                obj2.value = ConvertToWords(obj1.value);
            //alert("test");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <div id="deldialog" style="display: none" align="center">
    Do you want to delete this record?
</div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h5 align="center">&nbsp;<asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
            </h5>
        </div>

        <div class="panel-body">
            <div>

                <table align="center" width="100%">

                    <tr>
                        <td>
                            <asp:Label ID="lblHead" runat="server" Text="Head"></asp:Label></td>
                        <td>
                            <asp:DropDownList ID="DDLHead" runat="server"
                                OnSelectedIndexChanged="DDLHead_SelectedIndexChanged" Width="300px" CssClass="form-control"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="DDLHead" ErrorMessage="Enter Head"
                                ValidationGroup="1" ForeColor="Red">*</asp:RequiredFieldValidator></td>
                        <td>
                            <asp:Label ID="lblAmount" runat="server" Text="Sales Amount"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtAmount" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox></td>

                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblItaxRate" runat="server" Text="Income Tax Rate" ToolTip="4.5 /7.5/10"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItaxRate" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblItax1" runat="server" Text="Income Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItax1" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblServices" runat="server" Text="Services"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtServices" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Self"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblItaxRate2"  runat="server" Text="I.Tax Rate(Services)"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItaxRate2" Text="0" runat="server" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblItax" runat="server" Text="Income Tax(Services)"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtItax" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblStax" runat="server" Text="Sales Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtStax" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNetSTax" runat="server" Text="Net Sales Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNetSTax" runat="server" Text="0" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblPST" runat="server" Text="PST" ToolTip="Punjab Services Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtPST" runat="server" Text="0" Width="300px" CssClass="form-control"
                                ToolTip="Self"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBillAmount" runat="server" Text="Bill Amount"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtBillAmount" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblTotalTax" runat="server" Text="Total Tax"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTotalTax" Text="0" runat="server" Width="300px"
                                CssClass="form-control" ToolTip="Auto/Formula"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNetAmount" runat="server" Text="Net Amount"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNetAmount" Text="0" CssClass="form-control" runat="server"
                                Width="300px" ToolTip="Auto/Formula"></asp:TextBox></td>
                        <td>
                            <asp:Label ID="lblCbNo" runat="server" Text="CB #"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtCBNo" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>

                    </tr>
                   
                    <tr>
                        <td>
                            <asp:Label ID="lblOffCode" runat="server" Text="Office Code"></asp:Label></td>
                        <td>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <%--<input type="text" runat="server" id="officername" disabled="disabled" class="form-control" placeholder="Offcode..." />--%>
                                        <asp:TextBox ID="officername" runat="server" Width="190px" CssClass="form-control"></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-toggle="modal" data-target="#officers" data-keyboard="false" data-backdrop="static" style="cursor: pointer" type="button">Select Officer !</button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                        </td>
                        <td>
                            <asp:Label ID="lblPartyCode" runat="server" Text="Party Code"></asp:Label></td>
                        <td>
                            <%--<asp:DropDownList ID="DDLParty" runat="server" Width="300px" CssClass="form-control">
                </asp:DropDownList>--%>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <%--<input type="text" runat="server" id="officername" disabled="disabled" class="form-control" placeholder="Offcode..." />--%>
                                        <asp:TextBox ID="txtpartycode" runat="server" Width="190px" CssClass="form-control"></asp:TextBox>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" data-toggle="modal" data-target="#parties" data-keyboard="false" data-backdrop="static" style="cursor: pointer" type="button">Select Party !</button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBillno" runat="server" Text="Bill No."></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtBillNo" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblBillDate" runat="server" Text="Bill Date"></asp:Label></td>
                        <td>
                            <telerik:RadDateTimePicker ID="BillDate" Width="300px" CssClass="form-control" runat="server"></telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbldoSubtoAG" runat="server" Text="Sanction Date"></asp:Label></td>
                        <td>
                            <telerik:RadDateTimePicker ID="dosubtoag" runat="server" Width="300px" CssClass="form-control">
                            </telerik:RadDateTimePicker>
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Amount in Words"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtAmountinWords" runat="server" Width="300px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click1" class="btn btn-primary btn-lg" Width="85px"
                                Text="Save" />
                        </td>

                    </tr>
                     </table>
                   
                            <div class="dataTable_wrapper">
                            <asp:GridView ID="GVListShow" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="Vertical" PageSize="5"
                                OnRowCancelingEdit="GVListShow_RowCancelingEdit1"
                                OnRowEditing="GVListShow_RowEditing"
                                OnRowUpdating="GVListShow_RowUpdating"
                                OnSelectedIndexChanging="GVListShow_SelectedIndexChanging"
                                CssClass="table table-striped table-bordered table-hover"
                                OnRowCommand="GVListShow_RowCommand" ViewStateMode="Enabled"
                                OnPageIndexChanged="GVListShow_PageIndexChanged"
                                OnPageIndexChanging="GVListShow_PageIndexChanging" OnRowDeleting="GVListShow_RowDeleting">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Exp_Id" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Head">
                                        <ItemTemplate>
                                            <asp:Label ID="lblhead" runat="server" Text='<%#Eval("head")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("billamount")%>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" />

                                   </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PartyCode">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPartyCode" runat="server" Text='<%#Eval("partycode")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bill No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillNo" runat="server" Text='<%#Eval("billno")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Width="70px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DoBill">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldoBill" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ControlStyle Width="80px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CB Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcbNo" runat="server" Text='<%#Eval("cbno")%>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" />

                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="DOSubToAG">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldoSubtoAG" runat="server" Text='<%#Eval("dosubtoag","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="lnkbtnEdit" CommandName="Update" runat="server"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="LinkButton1" CommandName="edit"  CommandArgument='<%#Eval("exp_id") %>' runat="server"><img src="Images/graphic_design.png" border = "0" alt = "" /></asp:LinkButton>
                                        </ItemTemplate>
                                        <ControlStyle ForeColor="#006600" Font-Bold="False" Font-Size="Medium"
                                            Width="50px" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("exp_id") %>' OnClientClick="return confirm('are you sure to delete');">
                                                <img src="Images/DELBUTTON.png" border = "0" alt = "" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Cancel">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server"> <img 
                                    src="Images/stop.png" border = "0" alt = "" /> </asp:LinkButton>--%>
                                            <asp:LinkButton ID="LinkButton2" CommandName="Cancel" runat="server"><img 
                                    src="Images/stop.png" border = "0" alt = "" /></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />

                                    </asp:TemplateField>

                                </Columns> 
                                <PagerSettings FirstPageText=">>" LastPageText="<<" />
                                <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />

                                <EditRowStyle BackColor="#7C6F57" />
                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                <PagerSettings FirstPageText=">>" LastPageText="<<" />
                                <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />
                                <RowStyle BackColor="#E3EAEB" />
                                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                                <SortedAscendingHeaderStyle BackColor="#246B61" />
                                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                                <SortedDescendingHeaderStyle BackColor="#15524A" />

                               
                            </asp:GridView>
                                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="officers" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Select Office</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">

                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Employee Name</th>
                                            <th>Designation</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%

                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            //var officers = (from a in ent.v_AdminStaff.ToList().Where(x=> x.BPS != "LS" && x.BPS !=null && x.StatusId==3).Where(c=> int.Parse(c.BPS) >=16)
                                            var officers = (from a in ent.v_AdminStaff.ToList().Where(x => x.BPS != null && x.BPS != "LS" && x.StatusId == 3 && int.Parse(x.BPS) >= 16)
                                                            orderby a.OfficeId
                                                            select a).ToList();
                                            foreach (var abc in officers)
                                            { %>
                                        <tr>
                                            <td><%=abc.EmployeeId%></td>
                                            <td><%=abc.EmployeeName%></td>
                                            <td><%=abc.Desg%></td>
                                            <td><span class="input-group-btn">
                                                <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.EmployeeId %>)" type="button" data-dismiss="modal">Select!</button>
                                            </span></td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade bs-example-modal-lg" id="parties" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel1">Select Party</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">

                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table1">
                                    <thead>
                                        <tr>
                                            <th>Party Code</th>
                                            <th>Party Name</th>
                                            <th>Ph #</th>
                                            <th>Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%


                                            var Parties = (from a in ent.parties select a).ToList();

                                            foreach (var abc in Parties)
                                            { %>
                                        <tr>
                                            <td><%=abc.partycode%></td>
                                            <td><%=abc.name%></td>
                                            <td><%=abc.phno%></td>
                                            <td><span class="input-group-btn">
                                                <button id="selectoff1" class="btn btn-default" onclick="SelectParty(<%=abc.partycode %>)" type="button" data-dismiss="modal">Select!</button>
                                            </span></td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('#txtAmount').keyup(function () {

            $("#ContentPlaceHolder1_txtAmountinWords").val(ConvertToWords($("#ContentPlaceHolder1_txtAmount").val()));

        });


        $('#ContentPlaceHolder1_txtItaxRate').keyup(function () {

            //var tal = parseInt($(this).val()) || 0;
            //console.log("txtAmount: " + $("#ContentPlaceHolder1_txtAmount").val());
            //console.log("txtTax: " + $("#ContentPlaceHolder1_txtTax").val());
            
            if ($("#ContentPlaceHolder1_txtItaxRate").val().length > 0) {
                
                $("#ContentPlaceHolder1_txtItax1").val(parseFloat($("#ContentPlaceHolder1_txtAmount").val()) * parseFloat($("#ContentPlaceHolder1_txtItaxRate").val()) / 100);
                //$("#ContentPlaceHolder1_txtItaxRate2").val(parseFloat($("#ContentPlaceHolder1_txtAmount").val()) - parseFloat($("#ContentPlaceHolder1_txtTax").val()));
            } else
            {
                $("#ContentPlaceHolder1_txtItax1").val("0");
            }
        });

        $('#ContentPlaceHolder1_txtItaxRate2').keyup(function () {
            
                $("#ContentPlaceHolder1_txtItax").val(parseFloat($("#ContentPlaceHolder1_txtServices").val()) * parseFloat($("#ContentPlaceHolder1_txtItaxRate2").val()) / 100);
            

        });

        $('#ContentPlaceHolder1_txtStax').keyup(function () {

            $("#ContentPlaceHolder1_txtNetSTax").val(parseFloat($("#ContentPlaceHolder1_txtStax").val()) * 20 / 100);

        });

        $('#ContentPlaceHolder1_txtPST').keyup(function () {

            $("#ContentPlaceHolder1_txtBillAmount").val(parseFloat($('#ContentPlaceHolder1_txtAmount').val()) + parseFloat($('#ContentPlaceHolder1_txtServices').val()));
            $("#ContentPlaceHolder1_txtTotalTax").val(parseFloat($('#ContentPlaceHolder1_txtPST').val()) + parseFloat($('#ContentPlaceHolder1_txtNetSTax').val()) + parseFloat($('#ContentPlaceHolder1_txtItax').val()) + parseFloat($('#ContentPlaceHolder1_txtItax1').val()));
            $("#ContentPlaceHolder1_txtNetAmount").val(parseFloat($('#ContentPlaceHolder1_txtBillAmount').val()) - parseFloat($('#ContentPlaceHolder1_txtTotalTax').val()));
        });
    </script>
</asp:Content>
