﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CashierModel;

namespace CashierWeb.PAY
{
    public partial class AGStatusShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAGStatusData();

            }

        }

        private void LoadAGStatusData()
        {
            BudgetCMOEntities bd = new BudgetCMOEntities();
            var agstatus = (from a in bd.ags
                            orderby a.ag_id descending
                            select new { a.ag_id, a.cbno, a.totamount, a.tax, a.netclaim, a.dosubtoag, a.tokenno, a.doobj, a.status, a.dochqrec, a.chamount,a.chqno, a.remarks }).Take(5).ToList();
            Session["AGData"] = agstatus;
            DGVAGStatus.DataSource = agstatus;
            DGVAGStatus.DataBind();


        }

        protected void DGVAGStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVAGStatus.PageIndex = e.NewPageIndex;
            DGVAGStatus.DataBind();

        }

        protected void DGVAGStatus_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void DGVAGStatus_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Label SSid = (Label)DGVAGStatus.Rows[e.RowIndex].FindControl("lblAGId");
            int ssno = Convert.ToInt32(SSid.Text);
            Response.Redirect("AGStatus.aspx?ag_id=" + ssno);
            

        }

        protected void DGVAGStatus_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DGVAGStatus.EditIndex = e.NewEditIndex;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("AGStatus.aspx");
        }
    }
}