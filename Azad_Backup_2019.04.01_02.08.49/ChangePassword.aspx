﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Azad.ChangePassword" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Change Password</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
                                   <input class="form-control" placeholder="Username" runat="server" id="txtusername" type="text" required="required" autofocus="autofocus" />
       
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Old Password" runat="server"  id="txtPass" type="password" required="required" />
                                
      
                                </div>
                                <div class="form-group">
                                   <input class="form-control" placeholder="New Password" runat="server" id="txtNewPassword" type="text" required="required" autofocus="autofocus" />
       
                                </div>


                                 <asp:Button ID="btnSubmit" runat="server" Text="Change Password" 
              CssClass="btn btn-lg btn-success btn-block" 
            onclick="btnSubmit_Click" />
                            </fieldset>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
