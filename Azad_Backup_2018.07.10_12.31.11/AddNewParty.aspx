﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="AddNewParty.aspx.cs" Inherits="Azad.AddNewParty" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <%-- kendo.common.min.css contains common CSS rules used by all Kendo themes --%>
   
   <%-- <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>--%>
 <script type="text/javascript">
    var api_url = "/Api/PartiesApi.aspx";
    var party_id = 0;
     function Party_btnAdd_OnClick() {
         party_id = 0;
        $("#txtpartycode").val("");
        $("#txtName").val("");
        $("#txtAddress").val("");
    }

    function Party_btnSaveChanges_OnClick() {
        var data = {
            action: "save",
            partyid: party_id,
            partycode: $("#txtpartycode").val(),
            partyname: $("#txtName").val(),
            address: $("#txtAddress").val()
        };
        $.get(api_url, data, function (response) {
            var r = JSON.parse(response);
            
                location.reload();
            
        })
    }

    function Party_btnDelete_OnClick(id, name) {
        
        if (confirm("Are you sure to delete " + name + "?")) {
            $.get(api_url, { action: "delete", partyid: id }, function (response) {
                var r = JSON.parse(response);
                
                    location.reload();
                
            });
        }
    }

     function Party_btnEdit_OnClick(id) {
         $.get(api_url, { action: "get", partyid: id }, function (response) {
             var r = JSON.parse(response);
             $("#txtpartycode").val(r.partycode);
             $("#txtName").val(r.name);
             $("#txtAddress").val(r.address);
             party_id = id;
         });
     }
     
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <%--<div id="example">
            <div id="grid"></div>
        <script>
        $(function () {
            $("#grid").kendoGrid({
                height: 500,
                columns: [
                    { field: "partycode", width: "50px" },
                    { field: "name", width: "150px" },
                    { field: "mbno", width: "150px" },
                    { field: "address", width: "100px" },
                    { command: "destroy", title: "Delete", width: "110px" }
                ],
                filterable: {
                    mode: "row"
                },
                pageable: true,
                sortable: true,
                editable: true, // enable editing
                toolbar: ["create", "save", "cancel"], // specify toolbar commands
                dataSource: {
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // svc services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total", 
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "partycode",
                            fields: {
                                partycode: { editable: true, nullable: true },
                                name: { validation: { required: true }, type: "string" },
                                mbno: { type: "string", validation: { required: true } },
                                address: { type: "string" }
                            }
                        }
                    },
                    batch: true, // enable batch editing - changes will be saved when the user clicks the "Save changes" button
                    transport: {
                        create: {
                            url: "Products.svc/Create", //specify the URL which should create new records. This is the Create method of the Products.svc service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for svc
                        },
                        read: {
                            url: "Products.svc/Read", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for svc
                        },
                        update: {
                            url: "Products.svc/Update", //specify the URL from which should update the records. This is the Update method of the Products.svc service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for svc
                        },
                        destroy: {
                            url: "Products.svc/Destroy", //specify the URL which should destroy records. This is the Destroy method of the Products.svc service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for svc
                        },
                        parameterMap: function(data, operation) {
                            console.log(data);
                            if (operation != "read") {
                               
                                // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                                return JSON.stringify({ products: data.models })
                            } else {
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        });
    </script>
        </div>--%>
    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" onclick="Party_btnAdd_OnClick()" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info">Add New</button>
                        </div>
                        <div class="panel-body">
                             <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>Party Code</th>
                                            <th>Parrty Name</th>
                                            <th>Address</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%

                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            var parties = (from a in ent.parties select a).ToList();
                                            foreach (var abc in parties)
                                            { %>
                                        <tr>
                                            <td><%=abc.partycode%></td>
                                            <td><%=abc.name%></td>
                                            <td><%=abc.address%></td>
                                            <td>
                                           <%-- <td><span class="input-group-btn">
                                                <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.partycode %>)" type="button" data-dismiss="modal"><a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a></button>
                                            </span>--%>
                                                <button type="button" onclick="Party_btnEdit_OnClick(<%=abc.partyid%>)" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info btn-circle center-block"><i class="fa fa-edit"></i></button>
                                            </td>
                                            <td>
                                                <%--<a class="btn btn-social-icon btn-bitbucket" onclick="FillBilling(<%=abc.partycode %>)"><i class="fa fa-bitbucket"></i></a>--%>
                                                <button type="button" onclick="Party_btnDelete_OnClick(<%=abc.partyid %>,'<%=abc.name %>')" class="btn btn-danger btn-circle center-block"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="panel-footer">
                            Panel Footer
                        </div>
                    </div>

    <div class="col-md-4" style="z-index:9999;">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel">Parties Info</h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group m-b-40">
                                    <input type="text" class="form-control" id="txtpartycode" name="txtpartycode" required><span class="highlight"></span> <span class="bar"></span>
                                    <label for="txtpartycode">Party Code</label>
                                </div>
                                <div class="form-group m-b-40">
                                    <input type="text" class="form-control" id="txtName" name="txtName" required><span class="highlight"></span> <span class="bar"></span>
                                    <label for="txtName">Party Name</label>
                                </div>
                                <div class="form-group m-b-40">
                                    <input type="text" class="form-control" id="txtAddress" name="txtAddress" required><span class="highlight"></span> <span class="bar"></span>
                                    <label for="txtAddress">Address</label>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSaveChanges" class="btn btn-success waves-effect waves-light m-r-10" onclick="Party_btnSaveChanges_OnClick()">Save Changes</button>
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


    </div>   

</asp:Content>
