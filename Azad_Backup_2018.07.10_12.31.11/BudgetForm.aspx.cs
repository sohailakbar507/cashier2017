﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class BudgetForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //UploadGrid(date);
                int NextYear = DateTime.Now.Year + 1;
                int currentyear = DateTime.Now.Year;
                string BudgetYear = currentyear + "-" + NextYear;
                txtYear.Text = BudgetYear;
            }
        }
        private void UploadGrid(string year)
        {
            BudgetCMOEntities bujt = new BudgetCMOEntities();
            var buj = (from a in bujt.budgets
                       where a.fyear == year
                       select new { a.headid, a.head, a.majorhead, a.@object, a.budget1, a.supplygran, a.expens, a.fyear }).ToList();

            if (buj.Count() > 0)
            {
                //var data1 = (from a in tel.expenses where a.dobill.Value.Month == month && a.dobill.Value.Year == year select a).ToList();
                DGVTelephone.DataSource = buj;
                DGVTelephone.DataBind();
                return;
            }
            else
            {
                int ppyear = DateTime.Now.Year - 1;
                int NextYear = DateTime.Now.Year + 1;
                int currentyear = DateTime.Now.Year;
                string PreviousYear = ppyear + "-" + currentyear;
                string BudgetYear = currentyear + "-" + NextYear;
                var heads = (from a in bujt.budgets
                             where a.fyear == PreviousYear
                             select a).ToList();
                foreach (var abc in heads)
                {
                    BudgetCMOEntities ent = new BudgetCMOEntities();
                    budget rec = new budget();
                    rec.head = abc.head;
                    rec.majorhead = abc.majorhead;
                    rec.@object = abc.@object;
                    rec.budget1 = 0;
                    rec.supplygran = 0;
                    rec.expens = 0;
                    rec.fyear = BudgetYear;
                    rec.deptcode = abc.deptcode;
                    rec.ddopercode = abc.ddopercode;
                    rec.ddooffice = abc.ddooffice;
                    rec.ddoname = abc.ddoname;
                    rec.ddocode = abc.ddocode;
                    rec.date = new DateTime(currentyear, 7, 1);
                    rec.currdept = abc.currdept;
                    rec.currag = abc.currag;
                    rec.budget40p = abc.budget40p;
                    rec.ag = abc.ag;
                    rec.access = abc.access;
                    rec.abbr = abc.abbr;
                    rec.taxrate = abc.taxrate;
                    rec.sanction = abc.sanction;
                    rec.sanction2 = abc.sanction2;
                    rec.sanction3 = abc.sanction3;
                    rec.prevag = abc.prevag;
                    rec.prevdept = abc.prevdept;
                    rec.selection = abc.selection;
                    rec.surrender = abc.surrender;
                    rec.object1 = abc.object1;
                    ent.budgets.Add(rec);
                    ent.SaveChanges();

                }
            }
            var data = (from a in bujt.budgets
                        where a.fyear == year
                        orderby a.head
                        select new { a.headid, a.head, a.majorhead, a.@object, a.budget1, a.supplygran, a.expens, a.fyear }).ToList();
            DGVTelephone.DataSource = data;
            DGVTelephone.DataBind();
        }
        protected void DGVTelephone_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void DGVTelephone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVTelephone.PageIndex = e.NewPageIndex;
            DGVTelephone.DataSource = Session["TeleData"];
            DGVTelephone.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            foreach (GridViewRow row in DGVTelephone.Rows)
            {
                TextBox budget1 = (TextBox)row.FindControl("txtbudget");
                TextBox supplygran = (TextBox)row.FindControl("txtgrant");
                TextBox expens = (TextBox)row.FindControl("txtexpense");
                int bujid = Convert.ToInt32(DGVTelephone.DataKeys[row.RowIndex].Value);
                budget exp = new budget();
                budget update = (from a in tel.budgets where a.headid == bujid select a).First();
                if (budget1.Text != "")
                {
                    update.budget1 = Convert.ToDouble(budget1.Text);
                }
                if (supplygran.Text != "")
                {
                    update.supplygran = Convert.ToDouble(supplygran.Text);
                }
                if (expens.Text != "")
                {
                    update.expens = Convert.ToDouble(expens.Text);
                }

                tel.SaveChanges();

            }
        }

        protected void DGVTelephone_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void ViewDetail_Click(object sender, EventArgs e)
        {
            try
            {
                UploadGrid(txtYear.Text);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}