﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="BillsFinalGrid.aspx.cs" Inherits="Azad.BillsFinalGrid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style=" position:relative;left:0px; top:50px">
    
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="LinkBtnHome" runat="server" OnClick="LinkBtnHome_Click">Back</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <h1> Final List </h1>
                </td>
            </tr>
            <tr>
                <td>
                <asp:GridView ID="DGVFinalBill" runat="server" AutoGenerateColumns= "False" CellPadding="3" 
                    HorizontalAlign="Center" 
                    onrowediting="DGVFinalBill_RowEditing" PageSize="20" Width="384px" 
                    onpageindexchanging="DGVFinalBill_PageIndexChanging" 
                    onrowupdating="DGVFinalBill_RowUpdating" DataKeyNames="ag_id" AllowPaging="True" BorderStyle="None" BackColor="White" BorderColor="#999999" BorderWidth="1px" GridLines="Vertical">
                    <HeaderStyle CssClass="GridHeader" BackColor="#000084"></HeaderStyle>
                    <AlternatingRowStyle CssClass="GridAtlItem" BackColor="#DCDCDC" />

          <Columns>
              
              <asp:TemplateField HeaderText="AGId" Visible="false">
              <ItemTemplate>
                 <asp:Label ID="lblagId" runat="server" Text='<%#Eval("ag_id") %>'></asp:Label>
              </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EmpId">
              <ItemTemplate>
                 <asp:Label ID="lblEmpId" runat="server" Text='<%#Eval("EmployeeId") %>'></asp:Label>
              </ItemTemplate>
                        
              </asp:TemplateField>          
              <asp:TemplateField HeaderText="Personal No.">
              <ItemTemplate>
                  <asp:Label ID="lblPifra" runat="server" Text='<%#Eval("PIFRA") %>'></asp:Label>
                                
              </ItemTemplate>
                        
                  <ControlStyle Width="100px" />
                        
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Emp. Name">
                <ItemTemplate>
                   <asp:Label ID="lblEmpName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
                                         
                </ItemTemplate>
                    <ControlStyle Width="150px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                        
              <asp:TemplateField HeaderText="Amount">
              <ItemTemplate>
                 <asp:TextBox ID="txtAmountUpdated" runat="server" Text='<%#Eval("sum_amount") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="100px" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
              </asp:TemplateField>

              </Columns>
              
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="Black" HorizontalAlign="Center" BackColor="#999999" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                    


                </asp:GridView></td></tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save" Width="90px" 
                   onclick="btnSave_Click" />
                </td>

            </tr>
    </table></div>
</asp:Content>
