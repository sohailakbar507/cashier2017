﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BudgetMaster.Master" AutoEventWireup="true" CodeBehind="Telephone.aspx.cs" Inherits="Azad.Telephone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
</telerik:RadScriptManager>
    <br /><br />
    <div class="container">
  <div class="row">
   <div class="Absolute-Center is-Responsive">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                            <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="Telephone Entry Form" 
                            ForeColor="#000"></asp:Label>
                            </h5> 
                            
                        </div>
                        <div class="panel-body">
    <table align="center" style="border:01px dashed gray;border-collapse:collapse;" width="100%">
    <tr><td>
    Select Month :
     <%-- <input type="text" class="form-control" placeholder="Search for...">--%>
     <telerik:RadMonthYearPicker ID="RadMonthYearPicker1" runat="server" CssClass="form-control" 
                    onselecteddatechanged="RadMonthYearPicker1_SelectedDateChanged">
     </telerik:RadMonthYearPicker>   
     
     </td>
     <td>
        <span class="input-group-btn">
        <%--<button class="btn btn-default" type="button">Go!</button>--%>
        <asp:Button ID="ViewDetail" runat="server" class="btn btn-primary btn-small" 
        Text="NTC Lahore" onclick="ViewDetail_Click" Width="100px" />
        <asp:Button ID="btnIDB" runat="server" class="btn btn-primary btn-small" 
        Text="Islamabad" onclick="btnIDB_Click" Width="100px" />
        <asp:Button ID="btnPTCL" runat="server" class="btn btn-primary btn-small" 
        Text="PTCL" Width="100px" onclick="btnPTCL_Click" />
      </span></td></tr>
  
            <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="DGVTelephone" runat="server" AutoGenerateColumns= "False" CellPadding="4" 
                    HorizontalAlign="Center" 
                    onrowediting="DGVTelephone_RowEditing" PageSize="20" Width="384px" 
                    onpageindexchanging="DGVTelephone_PageIndexChanging" 
                    onrowupdating="DGVTelephone_RowUpdating" DataKeyNames="exp_id" 
                    ForeColor="#333333" GridLines="None">
                    <HeaderStyle CssClass="GridHeader" BackColor="#1C5E55"></HeaderStyle>
                    <AlternatingRowStyle CssClass="GridAtlItem" BackColor="White" />

          <Columns>
                <asp:TemplateField HeaderText="Sumid" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblSumid" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone No.">
                        <ItemTemplate>
                            <asp:Label ID="lblPhno" runat="server" Text='<%#Eval("descr") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="OffId">
                        <ItemTemplate>
                            <asp:Label ID="lblOffId" runat="server" Text='<%#Eval("offid") %>'></asp:Label>
                            </ItemTemplate>
                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtAmountUpdated" runat="server" Text='<%#Eval("amount") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                                           

                    </Columns>
                    

                    <EditRowStyle BackColor="#7C6F57" />
                    

                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" BackColor="#666666" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                    


                </asp:GridView>
            </td>
        </tr>
       
        </table>
         <br /><br />
        <table align="center" style="border:01px dashed gray;border-collapse:collapse;">
        <tr>
            <td align="center">

                <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save" Width="90px" 
                   onclick="btnSave_Click" />
                   
                <asp:Button ID="btnVerify" class="btn btn-primary btn-lg" runat="server" 
                    Text="Verify" Width="90px" 
                   onclick="btnVerify_Click" />
            </td>
        </tr>
    </table>
    </div></div></div></div></div>

</asp:Content>
