﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ShowForm.aspx.cs" Inherits="Azad.ShowForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <div>
        <h1 align = "center"> Show List </h1>
        <table style="border:1px dashed white;border-collapse:collapse;" align="center" width="600" cellpadding="2" cellspacing="1">
            <tr>
                <td><asp:Label ID="lblHead" runat="server" Text="Head"></asp:Label></td> 
                <td colspan="3"><asp:DropDownList ID="DDLHead" runat="server" Height="25px" 
                        Width="210px" onselectedindexchanged="DDLHead_SelectedIndexChanged">
                        <asp:ListItem Value = "0">-- Select Head --</asp:ListItem>
                        <asp:ListItem Value = "1">   Fuel & Electricty </asp:ListItem>
                        <asp:ListItem Value = "2">   Honorarium </asp:ListItem>
                        <asp:ListItem Value = "3">   Medical Charges </asp:ListItem>
                        <asp:ListItem Value = "4">   Postage & Telegraph </asp:ListItem>
                        <asp:ListItem Value = "5">   Telephone & Trunk Calls </asp:ListItem>
                        <asp:ListItem Value = "6">   Gas </asp:ListItem>

                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="GVCashier" runat="server" AutoGenerateColumns="False" 
                        BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="3" GridLines="Vertical" PageSize="5" 
                        onpageindexchanging="GVCashier_PageIndexChanging" AllowPaging="True" 
                        DataKeyNames="exp_id">
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                        <Columns>
                            <asp:TemplateField HeaderText="Head">
                            <ItemTemplate>
                                 <asp:Label ID="lblhead" runat="server" Text= '<%#Eval("head")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit">
                            <ItemTemplate>
                                 <asp:Label ID="lblUnit" runat="server" Text= '<%#Eval("unit")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                 <asp:Label ID="lblAmount" runat="server" Text= '<%#Eval("amount")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TaxRate">
                            <ItemTemplate>
                                 <asp:Label ID="lblTaxRate" runat="server" Text= '<%#Eval("taxrate")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IncomeTax">
                            <ItemTemplate>
                                 <asp:Label ID="lblItax" runat="server" Text= '<%#Eval("itax")%>'></asp:Label>
                            </ItemTemplate>
                            
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SalesTax">
                            <ItemTemplate>
                                 <asp:Label ID="lblSTax" runat="server" Text= '<%#Eval("stax")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tax">
                            <ItemTemplate>
                                 <asp:Label ID="lblTax" runat="server" Text= '<%#Eval("tax")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Difference">
                            <ItemTemplate>
                                 <asp:Label ID="lblDiff" runat="server" Text= '<%#Eval("diff")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OfficeCode">
                            <ItemTemplate>
                                 <asp:Label ID="lblOffCode" runat="server" Text= '<%#Eval("offcode")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PartyCode">
                            <ItemTemplate>
                                 <asp:Label ID="lblPartyCode" runat="server" Text= '<%#Eval("partycode")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BillNo">
                            <ItemTemplate>
                                 <asp:Label ID="lblBillNo" runat="server" Text= '<%#Eval("billno")%>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DateOfBill">
                            <ItemTemplate>
                                <asp:Label ID="lbldoBill" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CB #">
                            <ItemTemplate>
                                 <asp:Label ID="lblcbNo" runat="server" Text= '<%#Eval("cbno")%>'></asp:Label>
                            </ItemTemplate>
                            
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SanctionID">
                            <ItemTemplate>
                                 <asp:Label ID="lblSancId" runat="server" Text= '<%#Eval("Sanc_id")%>'></asp:Label>
                            </ItemTemplate>
                            
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit"></asp:TemplateField>
                            <asp:TemplateField HeaderText="Cancel"></asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
                    </asp:GridView>
                </td>
            </tr>
</table>
</div>
</asp:Content>
