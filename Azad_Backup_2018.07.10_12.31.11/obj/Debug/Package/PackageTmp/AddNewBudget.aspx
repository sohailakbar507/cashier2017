﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="AddNewBudget.aspx.cs" Inherits="Azad.AddNewBudget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

       <%-- <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>--%>
 <script type="text/javascript">
    var api_url = "/Api/BudgetAPI.aspx";
    var head_id = 0;
     function Budget_btnAdd_OnClick() {
         head_id = 0;
        $("#txthead").val("");
        $("#txtObject").val("");
        $("#txtbudget").val("");
        $("#txtexpens").val("");
        $("#txtsupplgran").val("");
     }

     function Budget_btnSaveChanges_OnClick() {
         var data = {
             action: "save",
             headid: head_id,
             head: $("#txthead").val(),
             object1: $("#txtObject").val(),
             budget1: $("#txtbudget").val(),
             expens: $("#txtexpens").val(),
             supplygran: $("#txtsupplgran").val()

         };
         $.get(api_url, data, function (response) {
             var r = JSON.parse(response);

             location.reload();

         })
     }

     function Budget_btnDelete_OnClick(id, name) {

         if (confirm("Are you sure to delete " + name + "?")) {
             $.get(api_url, { action: "delete", headid: id }, function (response) {
                 var r = JSON.parse(response);

                 location.reload();

             });
         }
     }
     function Budget_btnEdit_OnClick(id) {
         $.get(api_url, { action: "get", headid: id }, function (response) {
             var r = JSON.parse(response);
             console.log(r);
             $("#txthead").val(r.head);
             $("#txtObject").val(r.object1);
             $("#txtbudget").val(r.budget1);
             $("#txtexpens").val(r.expens);
             $("#txtsupplgran").val(r.supplygran);
             head_id = id;
         });
     }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" onclick="Budget_btnAdd_OnClick()" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info">Add New</button>
                        </div>
                        <div class="panel-body">
                             <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>head</th>
                                            <th>Object</th>
                                            <th>Budget</th>
                                            <th>Expense</th>
                                            <th>Supply Grant</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%

                                            CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                            var bujt = (from a in ent.budgets select a).ToList();
                                            foreach (var abc in bujt)
                                            { %>
                                        <tr>
                                            <td><%=abc.head%></td>
                                            <td><%=abc.object1%></td>
                                            <td><%=abc.budget1%></td>
                                            <td><%=abc.expens%></td>
                                            <td><%=abc.supplygran%></td>

                                            <td>
                                           <%-- <td><span class="input-group-btn">
                                                <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.partycode %>)" type="button" data-dismiss="modal"><a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a></button>
                                            </span>--%>
                                                <button type="button" onclick="Budget_btnEdit_OnClick(<%=abc.headid%>)" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info btn-circle center-block"><i class="fa fa-edit"></i></button>
                                            </td>
                                            <td>
                                                <%--<a class="btn btn-social-icon btn-bitbucket" onclick="FillBilling(<%=abc.partycode %>)"><i class="fa fa-bitbucket"></i></a>--%>
                                                <button type="button" onclick="Budget_btnDelete_OnClick(<%=abc.headid %>,'<%=abc.head %>')" class="btn btn-danger btn-circle center-block"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="panel-footer">
                            Panel Footer
                        </div>
                    </div>

    <div class="col-md-4" style="z-index:9999;">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel">Heads Info</h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group m-b-40">
                                     <label for="txthead">Head</label>
                                    <input type="text" class="form-control" id="txthead" name="txthead" required><span class="highlight"></span> <span class="bar"></span>
                                   
                                </div>
                                <div class="form-group m-b-40">
                                    <label for="txtObject">Head Name</label>
                                    <input type="text" class="form-control" id="txtObject" name="txtObject" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                                <div class="form-group m-b-40">
                                     <label for="txtbudget">Budget</label>
                                    <input type="text" class="form-control" id="txtbudget" name="txtbudget" required><span class="highlight"></span> <span class="bar"></span>
                                   
                                </div>
                                 <div class="form-group m-b-40">
                                      <label for="txtexpens">Expense</label>
                                    <input type="text" class="form-control" id="txtexpens" name="txtexpens" required><span class="highlight"></span> <span class="bar"></span>
                                   
                                </div>
                                 <div class="form-group m-b-40">
                                     <label for="txtsupplgran">Supply Grant</label>
                                    <input type="text" class="form-control" id="txtsupplgran" name="txtsupplgran" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSaveChanges" class="btn btn-success waves-effect waves-light m-r-10" onclick="Budget_btnSaveChanges_OnClick()">Save Changes</button>
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


    </div>
</asp:Content>
