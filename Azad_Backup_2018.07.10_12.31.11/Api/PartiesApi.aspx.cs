﻿using CashierModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad.Api
{
    public partial class PartiesApi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int party_id = 0;
                BudgetCMOEntities ent = new BudgetCMOEntities();
                party response = new party();
                string id = Request["partyid"];
                Response.Headers["Content-type"] = "application/json";
                if (string.IsNullOrEmpty(Request["action"]))
                {
                    string r = JsonConvert.SerializeObject("No action defined");
                    Response.Write(r);
                    return;
                }

                if (string.IsNullOrEmpty(Request["partyid"]))
                    party_id = 0;
                else
                    int.TryParse(Request["partyid"], out party_id);

                switch (Request["action"])
                {
                    case "save":
                        if (party_id == 0)
                        {
                            response = new party();
                            response.partycode = Convert.ToInt32(Request["partycode"]);
                            response.name = Request["partyname"];
                            response.address = Request["address"];
                            ent.parties.Add(response);
                            ent.SaveChanges();
                        }

                        else
                        {

                            response = (from a in ent.parties where a.partyid == party_id select a).First();
                            response.name = Request["partyname"];
                            response.address = Request["address"];
                            ent.SaveChanges();
                        }

                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                    case "delete":
                        response = (from a in ent.parties where a.partyid == party_id select a).First();
                        ent.parties.Remove(response);
                        ent.SaveChanges();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;
                    case "get":
                        response = (from a in ent.parties where a.partyid == party_id select a).First();
                        Response.Write(JsonConvert.SerializeObject(response));
                        break;

                }
            }
        }
    }
}