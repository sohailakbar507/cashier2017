﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ReportsPage.aspx.cs" Inherits="Azad.ReportsPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
    .myTable > tbody > tr > td > label
    {
        font-weight:normal;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div style=" position:relative;left:500px; top:50px">
<table>
    <tr>
        <td>
            <h1> Cashier Reports </h1>
        </td>
    </tr>
</table></div>
<div style="width: 100%; height: 100%;">
    <div style="background-color: #f7f7f9; border: 1px solid #e1e1e8; width:100%; height: 500px; position:relative; left: 300px;top: 70px;">
        
        <div class="row">
            <div class="col-md-6" style="border-right: 1px solid #e1e1e8; height: 500px">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Report
                    <span class="glyphicon glyphicon-chevron-down"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                          
                          <%if (!this.IsPostBack)
                            {
                                if (Session["login"] != null)
                                {
                                    CashierModel.BudgetCMOEntities ent = new CashierModel.BudgetCMOEntities();
                                    CashierModel.Cls_Login login = (CashierModel.Cls_Login)Session["login"];
                                    var checkReportsRights = from a in ent.MenuAccessRights where a.GroupId == login.Groupid select a;
                                    if (checkReportsRights.Count() > 0)
                                    {
                                        
                                        foreach (var m in checkReportsRights)
                                        {
                                            var menuname1 = (from a in ent.menuRights where a.menu_id == m.M_ID && a.IsReport == true select a);
                                            if (menuname1.Count() > 0)
                                            {
                                                var menuname = menuname1.First();
                                            
                                            %>
                                             <li><a href="<%=this.ResolveUrl(menuname.Url) %>"><%=menuname.MenuName%></a></li>
                                                 <%
                                            }
                                        }
                                        
                                    }                                    

                                }
                                else
                                {
                                    Response.Redirect("~/LoginForm.aspx");
                                }
                            } %> 
                            
                    </ul>
                    
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                    Reference/CB #
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        
                        <telerik:RadComboBox ID="rcbCBNo" Runat="server" Filter="StartsWith" CssClass="form-control">
                        </telerik:RadComboBox>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-12">
                         <asp:TextBox ID="txtYear" Width="250" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblError" runat="server" or="Red"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RadioButtonList ID="rblReportCategories" runat="server" 
                            CssClass="myTable table table-hover" 
                            onselectedindexchanged="rblReportCategories_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
