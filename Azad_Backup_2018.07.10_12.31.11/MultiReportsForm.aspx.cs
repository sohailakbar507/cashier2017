﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class MultiReportsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {                }
            }

        }

        private void LoadRecords()
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            Cls_Login login = (Cls_Login)Session["login"];
            DateTime Fromdate = Convert.ToDateTime(MultiDatePicker.DateInput.SelectedDate);
            var abc = (from a in cmo.expenses
                       where a.fyear == login.fyear && a.UserName == login.Username && a.date == Fromdate
                       orderby a.exp_id descending
                       select new { a.exp_id, a.date, a.head, a.cbno, a.amount, a.fyear }).ToList();
            var sorted_abc = (from a in abc
                              orderby int.Parse(a.cbno.Substring(3))
                              select a
               ).ToList();
            DGVMultiBudget.DataSource = sorted_abc;
            DGVMultiBudget.DataBind();
            Session["dtData"] = sorted_abc;           
        }

        protected void lnkShowData_Click(object sender, EventArgs e)
        {
            LoadRecords();
        }

        protected void LinkPrint_Click(object sender, EventArgs e)
        {
            string report = Request["reportid"];
            if (report == "mbp")
            { 
                Response.Redirect("ViewSTIReports.aspx?reportid=mbp&mdate=" + MultiDatePicker.SelectedDate.ToString());
            }
            if (report == "MultiSanc")
            {
                Response.Redirect("ViewSTIReports.aspx?reportid=MultiSanc&mdate=" + MultiDatePicker.SelectedDate.ToString());
            }
            if (report == "MultiCompSheet")
            {
                Response.Redirect("ViewSTIReports.aspx?reportid=MultiCompSheet&mdate=" + MultiDatePicker.SelectedDate.ToString());
            }
           

        }

    }
}