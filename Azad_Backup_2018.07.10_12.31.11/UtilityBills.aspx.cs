﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class UtilityBills : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    BudgetCMOEntities CMO = new BudgetCMOEntities();
                    var heads = (from a in CMO.Sub_BudgetCategories where (a.ID == 3 || a.ID == 7) select a).ToList();
                    DDLHead.DataSource = heads;
                    DDLHead.DataTextField = "SubCatName";
                    DDLHead.DataValueField = "SubCatCode";
                    DDLHead.DataBind();
                    getOfficers();
                    LoadList();

                }
                else
                {
                    // Response.Redirect("~/LoginForm.aspx");
                }
            }

        }
        private void EditRecord(int id)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            int expid = Convert.ToInt32(id);
            var abc = (from a in cmo.expenses
                       where a.exp_id == expid
                       select new { a.exp_id, a.head, a.unit, a.amount, a.bilrefence, a.offcode, a.dobill, a.cbno, a.remarks, a.name, a.dosubtoag }).Take(5).ToList();
            if (abc.Count() > 0)
            {
                var x = abc.First();
                DDLHead.SelectedIndex = DDLHead.Items.IndexOf(DDLHead.Items.FindByValue(x.head.ToString()));
                txtUnit.Text = x.unit.ToString();
                txtAmount.Text = x.amount.ToString();
                officername.Text = x.offcode.ToString();
                txtName.Text = x.name;
                txtBillRef.Text = x.bilrefence;
                BillDate.DateInput.DisplayText = x.dobill.ToString();
                txtRemarks.Text = x.remarks;
                txtCBNo.Text = x.cbno;


            }

        }

        private void LoadList()
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            string headz = (DDLHead.SelectedItem.Value);
            var abc = (from a in cmo.expenses
                       where a.head == headz
                       orderby a.exp_id descending
                       select new { a.exp_id, a.head, a.unit, a.amount, a.offcode, a.bilrefence, a.dobill, a.cbno, a.name, a.remarks, a.Sanc_id, a.dosubtoag }).Take(5).ToList();
            //if (abc.Count() > 0)
            //{
            //    var name = abc.First();
            //    txtName.Text = name.name;
            //}
            GVListShow.DataSource = abc;
            GVListShow.DataBind();

        }

        private void getOfficers()
        {
            BudgetCMOEntities CMO = new BudgetCMOEntities();
            var officer = (from a in CMO.v_AdminStaff orderby a.EmployeeId select a).ToList();

        }

        protected void DDLHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            //int cashid = Convert.ToInt32(Request["exp_id"]);

            string headz = (DDLHead.SelectedItem.Value);
            var abc = (from a in cmo.expenses
                       from b in cmo.Sub_BudgetCategories
                       where b.SubCatCode == headz && a.head == b.SubCatCode
                       orderby a.exp_id descending
                       select new { a.exp_id, a.head, a.unit, a.amount, a.offcode, a.dobill, a.name, a.cbno, a.remarks, a.bilrefence, a.Sanc_id, a.dosubtoag }).Take(5).ToList();

            if (abc.Count() > 0)
            {
                GVListShow.DataSource = abc;
                GVListShow.DataBind();

            }
        }

        expense DataMapping(expense ef)
        {
            //expense ef = new expense();
            return ef;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
        }

        protected void GVListShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void GVListShow_RowCancelingEdit1(object sender, GridViewCancelEditEventArgs e)
        {
        }
        protected void GVListShow_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVListShow.EditIndex = e.NewEditIndex;
            Session["edit"] = 1;
        }

        protected void GVListShow_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GVListShow.PageIndex = e.NewSelectedIndex;
            GVListShow.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            if (Session["edit"] == null)
            {
                Cls_Login login = (Cls_Login)Session["Login"];
                if (Convert.ToDateTime(DateTime.Now.ToShortDateString()) >= login.yearstart && Convert.ToDateTime(DateTime.Now.ToShortDateString()) <= login.yearend)
                {
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Date is invalid please check date and try again.');", true);
                    return;
                }

                expense ef = new expense();
                ef.head = DDLHead.SelectedItem.Value;
                ef.unit = txtUnit.Text == string.Empty ? 0 : Convert.ToDouble(txtUnit.Text);
                ef.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtAmount.Text);
                ef.offcode = Convert.ToInt32(officername.Text);
                ef.dobill = BillDate.SelectedDate;
                ef.bilrefence = txtBillRef.Text;
                v_AdminStaff admn = (from x in cmo.v_AdminStaff where x.EmployeeId == ef.offcode select x).First();
                ef.name = admn == null ? "" : admn.EmployeeName + "," + admn.Desg;
                ef.cbno = txtCBNo.Text;
                ef.fyear = login.fyear;
                ef.remarks = txtRemarks.Text;
                ef.UserName = "Shabbir";
                cmo.expenses.Add(ef);
                cmo.SaveChanges();

                LoadList();
            }
            else
            {
                int cashid = Convert.ToInt32(Session["pram"]);
                //expense E = new expense();
                var E = (from a in cmo.expenses where a.exp_id == cashid select a).First();
                E.head = DDLHead.SelectedItem.Value;
                E.unit = txtUnit.Text == string.Empty ? 0 : Convert.ToInt32(txtUnit.Text);
                E.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtAmount.Text);
                E.offcode = Convert.ToInt32(officername.Text);
                E.bilrefence = txtBillRef.Text;
                E.dobill = BillDate.SelectedDate;
                v_AdminStaff admn = (from x in cmo.v_AdminStaff where x.EmployeeId == E.offcode select x).First();
                E.name = admn == null ? "" : admn.EmployeeName + "," + admn.Desg;
                E.cbno = txtCBNo.Text;
                E.remarks = txtRemarks.Text;
                cmo.SaveChanges();
                Session["edit"] = null;
                Session["pram"] = null;
                LoadList();
            }
        }

        protected void GVListShow_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                // Label SSid = (Label)GVListShow.Rows[e.RowIndex].FindControl("lblExpId");
                EditRecord(Convert.ToInt32(e.CommandArgument));
                Session["edit"] = 1;
                Session["pram"] = e.CommandArgument;
            }

            if (e.CommandName == "Cancel")
            {
                Response.Redirect("DashBoard.aspx");
            }
        }

    }
}
