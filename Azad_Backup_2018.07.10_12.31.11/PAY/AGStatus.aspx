﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CashierMain.Master" AutoEventWireup="true" CodeBehind="AGStatus.aspx.cs" Inherits="Azad.Pay.AGStatus" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script language="javascript">


        function OnFiguresChange() {
            var obj1 = document.getElementById('<%=txtNetClaim.ClientID %>');
            var obj2 = document.getElementById('<%=txtAmntinWords.ClientID %>');
            obj2.value = ConvertToWords(obj1.value);
            //alert("test");
        }
    function GetWords(figure) {
         var singles = new Array();
         singles[0] = "";
         singles[1] = "One";
         singles[2] = "Two";
         singles[3] = "Three";
         singles[4] = "Four";
         singles[5] = "Five";
         singles[6] = "Six";
         singles[7] = "Seven";
         singles[8] = "Eight";
         singles[9] = "Nine";
         singles[10] = "Ten";
         singles[11] = "Eleven";
         singles[12] = "Twelve";
         singles[13] = "Thirteen";
         singles[14] = "Fourteen";
         singles[15] = "Fifteen";
         singles[16] = "Sixteen";
         singles[17] = "Seventeen";
         singles[18] = "Eighteen";
         singles[19] = "Nineteen";
         singles[20] = "Twenty";

         var tens = new Array();
         tens[2] = "Twenty";
         tens[3] = "Thrity";
         tens[4] = "Forty";
         tens[5] = "Fifty";
         tens[6] = "Sixty";
         tens[7] = "Seventy";
         tens[8] = "Eighty";
         tens[9] = "Ninty";

         if (figure <= 20) { return singles[figure]; }
         else { return tens[Math.floor(figure / 10)] + " " + singles[figure % 10]; }
     }
     function ConvertToWords(amount) {
         var units = new Array();
         units[0] = "Hundred";
         units[1] = "Thousand";
         units[2] = "Lacs";
         units[3] = "Crore";
         var returnValue = "";


         var intTens = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inHunderds = amount % 10;
         amount /= 10;
         amount = Math.floor(amount);

         var inThousands = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inLacs = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inCrores = amount;

         if (inCrores > 0) {
             returnValue += " " + GetWords(inCrores) + " " + units[3];
         }

         if (inLacs > 0) {
             returnValue += " " + GetWords(inLacs) + " " + units[2];
         }

         if (inThousands > 0) {
             returnValue += " " + GetWords(inThousands) + " " + units[1];
         }

         if (inHunderds > 0) {
             returnValue += " " + GetWords(inHunderds) + " " + units[0];
         }

         if (intTens > 0) {
             if (returnValue != "") {
                 returnValue += " &";
             }
             returnValue += " " + GetWords(intTens);
         }

         return returnValue;
     }
    </script>
     <div class="panel panel-default">

                        <div class="panel-heading">
                        <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="Follow up with AG and Disbursement" 
                ForeColor="#000066"></asp:Label>
        </h5>
                        </div>
                        
                        <div class="panel-body">
    
        
        <table align="center" width="100%">
        <tr>
            <td>
                <asp:Label ID="lblCbNO" runat="server" Text="CBNo"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCBNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblcbAmount" runat="server" Text="CB Amount"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCBAmount" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTax" runat="server" Text="Tax"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTax" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblNetClaim" runat="server" Text="Net Claim"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNetClaim" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td></td>
            <td colspan="4">
                <asp:TextBox ID="txtAmntinWords" runat="server" Width="510px" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDateAg" runat="server" Text="Date-Submission-AG"></asp:Label>
            </td>
            <td>
                <telerik:RadDateTimePicker ID="RDSubtoAG" runat="server" CssClass="form-control">
                </telerik:RadDateTimePicker>
            </td>
            <td>
                <asp:Label ID="lblTokenNo" runat="server" Text="Token #"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTokenNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblObjectionDate" runat="server" Text="DateofObjection"></asp:Label>
            </td>
            <td>
                <telerik:RadDateTimePicker ID="RDObjection" runat="server" CssClass="form-control">
                </telerik:RadDateTimePicker>
            </td>
            <td>
                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DDLStatus"  runat="server" CssClass="form-control">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblChqDate" runat="server" Text="Cheque Date"></asp:Label>
            </td>
            <td>
                <telerik:RadDateTimePicker ID="RDChqDate" runat="server" CssClass="form-control">
                </telerik:RadDateTimePicker>
            </td>
                
            <td>
                <asp:Label ID="lblChqNo" runat="server" Text="Cheque No"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtChqNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"></asp:TextBox>
            </td>


        </tr>
        <tr>
            <td>
                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="510px" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
        
            <td colspan="4" align="center">
              <br />    <asp:Button ID="btnAdd" runat="server" class="btn btn-primary btn-lg" Text="Add" Width="85px" 
                    onclick="btnAdd_Click" />
                        <asp:Button ID="btnSave" runat="server" class="btn btn-primary btn-lg" Text="Save" Width="85px" 
                    onclick="btnSave_Click"/>
            </td>
        </tr>


</table></div>
</div>

</asp:Content>
