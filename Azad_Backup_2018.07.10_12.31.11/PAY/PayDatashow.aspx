﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="PayDatashow.aspx.cs" Inherits="CashierWeb.PAY.PayDatashow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
<div class="panel panel-default">
 <div class="panel-heading">
                        <h5 align = "left"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="PAY Form List" 
                ForeColor="#000066"></asp:Label>
        </h5>
                        </div>
                        
                        <div class="panel-body">
    <table>
       
        <tr>
            <td align = "center">
                <asp:GridView ID="DGVPayForm" runat="server" AllowPaging="True" CellPadding="4" 
                    ForeColor="#333333" GridLines="Vertical" AutoGenerateColumns="False" 
                    onpageindexchanging="DGVPayForm_PageIndexChanging" PageSize="5" 
                    onrowcancelingedit="DGVPayForm_RowCancelingEdit" 
                    onrowupdating="DGVPayForm_RowUpdating" 
                    onrowediting="DGVPayForm_RowEditing">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="ExpID" Visible = "false">
                            <ItemTemplate>
                            <asp:Label ID="lblExpId" runat="server" Text='<%#Eval("exp_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("date","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Head">
                        <ItemTemplate>
                            <asp:Label ID="lblHead" runat="server" Text= '<%#Eval("head")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descirption">
                        <ItemTemplate>
                            <asp:Label ID="lblDescr" runat="server" Text= '<%#Eval("descr")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text= '<%#Eval("amount")%>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bill Date">
                        <ItemTemplate>
                            <asp:Label ID="lblBillDate" runat="server" Text='<%#Eval("dobill","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bill Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblBillAmount" runat="server" Text= '<%#Eval("billamount")%>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deduction">
                        <ItemTemplate>
                            <asp:Label ID="lblDeduction" runat="server" Text= '<%#Eval("tax")%>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CB No">
                        <ItemTemplate>
                            <asp:Label ID="lblCBNo" runat="server" Text= '<%#Eval("cbno")%>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text= '<%#Eval("remarks")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnEdit" CommandName="Update" runat="server"><img src="../Images/edit-notes.png" border = "0" alt = "" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cancel">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkBtnCancel" CommandName = "Cancel" runat="server"> <img src="../Images/Cancel.png" border = "0" alt = ""/> </asp:LinkButton>
                         </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>        
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:LinkButton ID="btnBack" runat="server" onclick="btnBack_Click" 
                    ToolTip="Back"><img src="../Images/back-button-hi.png" alt="" /></asp:LinkButton>
            </td>
        </tr>
    </table>
    </div>
</div></div>
    
</asp:Content>
