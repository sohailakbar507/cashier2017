﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AGStatusShow.aspx.cs" Inherits="CashierWeb.PAY.AGStatusShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
<div class="panel panel-default">

                        <div class="panel-heading">
                        <h5 align = "left"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="AG Status List" 
                ForeColor="#CC0E28"></asp:Label>
        </h5>
                        </div>
                        
                        <div class="panel-body">
    <table>
       
        <tr>
            <td align = "center">
                <asp:GridView ID="DGVAGStatus" runat="server" AllowPaging="True" CellPadding="4" 
                    ForeColor="Black" GridLines="Vertical" AutoGenerateColumns="False" 
                    onpageindexchanging="DGVAGStatus_PageIndexChanging" PageSize="5" 
                    onrowcancelingedit="DGVAGStatus_RowCancelingEdit" 
                    onrowupdating="DGVAGStatus_RowUpdating" 
                    onrowediting="DGVAGStatus_RowEditing" BackColor="White" 
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="AGID" Visible = "false">
                            <ItemTemplate>
                            <asp:Label ID="lblAGId" runat="server" Text='<%#Eval("ag_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CBNo" Visible = "true">
                            <ItemTemplate>
                            <asp:Label ID="lblCBNo" runat="server" Text='<%#Eval("cbno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CBAmount" Visible = "true">
                            <ItemTemplate>
                            <asp:Label ID="lblCBAmount" runat="server" Text='<%#Eval("totamount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tax" Visible = "true">
                            <ItemTemplate>
                            <asp:Label ID="lblTax" runat="server" Text='<%#Eval("tax") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net Claim" Visible = "true">
                            <ItemTemplate>
                            <asp:Label ID="lblNetClaim" runat="server" Text='<%#Eval("netclaim") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="AGDate">
                        <ItemTemplate>
                            <asp:Label ID="lblAGDate" runat="server" Text='<%#Eval("dosubtoag","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Token No" Visible = "true">
                            <ItemTemplate>
                            <asp:Label ID="lblTokenNo" runat="server" Text='<%#Eval("tokenno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ObjectionDate">
                        <ItemTemplate>
                            <asp:Label ID="lblObjDate" runat="server" Text='<%#Eval("doobj","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cheque No">
                        <ItemTemplate>
                            <asp:Label ID="lblChqNo" runat="server" Text= '<%#Eval("chqno")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ChequeDate">
                        <ItemTemplate>
                            <asp:Label ID="lblChqDate" runat="server" Text='<%#Eval("dochqrec","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text= '<%#Eval("status")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text= '<%#Eval("chamount")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text= '<%#Eval("remarks")%>'></asp:Label>
                        </ItemTemplate>
                            <ControlStyle Width="100px" />
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnEdit" CommandName="Update" runat="server"><img src="../Images/edit-notes.png" border = "0" alt = "" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cancel">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkBtnCancel" CommandName = "Cancel" runat="server"> <img src="../Images/Cancel.png" border = "0" alt = ""/> </asp:LinkButton>
                         </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                    <SortedAscendingHeaderStyle BackColor="#848384" />
                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                    <SortedDescendingHeaderStyle BackColor="#575357" />
                </asp:GridView>        
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:LinkButton ID="btnBack" runat="server" onclick="btnBack_Click" 
                    ToolTip="Back"><img src="../Images/back-button-hi.png" alt="" /></asp:LinkButton>
            </td>
        </tr>
    </table>
    </div>
</div></div>

</asp:Content>
