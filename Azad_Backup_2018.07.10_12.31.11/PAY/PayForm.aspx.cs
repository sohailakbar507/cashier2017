﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CashierModel;

namespace Azad.Pay
{
    public partial class PayForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["login"] != null)
                {

                    txtEmpId.Enabled = false;
                    txtBillAmount.Enabled = false;
                    loadHead();
                    loadOfficer();
                    LoadList();


                }
            }

        }

        private void LoadList()
        {
            int catid = Convert.ToInt32(Request.QueryString["catid"]);
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            Cls_Login login = (Cls_Login)Session["login"];
            // string headcode = DDLHead.SelectedValue;

            var abc = (from a in cmo.expenses
                       where a.fyear == login.fyear && a.UserName == login.Username
                       orderby a.exp_id descending
                       select new { a.exp_id, a.date, a.head, a.amount,a.dobill,a.cbno,a.remarks,a.tax }).ToList();

            DGVPayForm.DataSource = abc;
            DGVPayForm.DataBind();
            Session["dtData"] = abc;

        }
        expense DataMapping(expense ef)
        {
            //expense ef = new expense();
            return ef;
        }

        private void EditRecord(int id)
         {
            BudgetCMOEntities bge = new BudgetCMOEntities();
            //int expid = Convert.ToInt32(id);
            int expid = Convert.ToInt32(id);
            var exp = (from a in bge.expenses
                       where a.exp_id==expid
                       select new { a.exp_id, a.date, a.head, a.descr,a.offcode, a.amount, a.dobill, a.cbno, a.remarks,a.tax }).ToList();
            if (exp.Count() > 0)
            {
                var abc = exp.First();
                RDDate.DateInput.DisplayText = abc.date.ToString();
                DDLHead.SelectedIndex = DDLHead.Items.IndexOf(DDLHead.Items.FindByValue(abc.head.ToString()));
                txtDescr.Text = abc.descr;
                txtDeduction.Text = Convert.ToDouble(abc.tax).ToString();
                officername.Text = abc.offcode.ToString();
                txtPayAmount.Text = Convert.ToDouble(abc.amount).ToString();
                RDDateofBill.DateInput.DisplayText = abc.dobill.ToString();
                txtCBNo.Text = abc.cbno;
                txtRemarks.Text = abc.remarks;
                
            }
                     

            }

        private void loadHead()
        {
            BudgetCMOEntities bg = new BudgetCMOEntities();
            int catidsss = Convert.ToInt32(Request["catid"]);
            var head = (from h in bg.Sub_BudgetCategories where h.CatID == catidsss orderby h.SubCatName select h).ToList();
            DDLHead.DataSource = head;
            DDLHead.DataValueField = "SubCatCode";
            DDLHead.DataTextField = "SubCatName";
            DDLHead.DataBind();
            
        }
        private void loadOfficer()
        {
            BudgetCMOEntities bg = new BudgetCMOEntities();
            var officer = (from a in bg.v_AdminStaff orderby a.EmployeeId select a).ToList();
                       
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            if (Session["edit"] == null)
            {
                Cls_Login login = (Cls_Login)Session["Login"];
                if (Convert.ToDateTime(DateTime.Now.ToShortDateString()) >= login.yearstart && Convert.ToDateTime(DateTime.Now.ToShortDateString()) <= login.yearend)
                {
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Date is invalid please check date and try again.');", true);
                    return;
                }

                expense ef = new expense();
                ef.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                ef.head = DDLHead.SelectedItem.Value;
                ef.amount = txtPayAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtPayAmount.Text);
                ef.offcode = officername.Text == string.Empty ? 0 : Convert.ToInt32(officername.Text);
                ef.tax = txtDeduction.Text == string.Empty ? 0 : Convert.ToDouble(txtDeduction.Text);
                ef.dobill = RDDateofBill.SelectedDate;
                ef.cbno = txtCBNo.Text;
                ef.fyear = login.fyear;
                ef.remarks = txtRemarks.Text;
                ef.UserName = login.Username;
                cmo.expenses.Add(ef);
                cmo.SaveChanges();
                LoadList();
            }
            else
            {
                int cashid = Convert.ToInt32(Session["pram"]);

                expense E = new expense();
                E = (from a in cmo.expenses where a.exp_id == cashid select a).First();
                E.date = DateTime.Now;
                E.head = DDLHead.SelectedItem.Value;
                E.amount = txtPayAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtPayAmount.Text);
                E.offcode = Convert.ToInt32(officername.Text);
                E.dobill = RDDateofBill.SelectedDate;
                E.tax = txtDeduction.Text == string.Empty ? 0 : Convert.ToDouble(txtDeduction.Text);
                E.cbno = txtCBNo.Text;
                E.remarks = txtRemarks.Text;
                cmo.SaveChanges();
                Session["edit"] = null;
                Session["pram"] = null;
                LoadList();
            }
        }

        protected void DGVPayForm_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVPayForm.PageIndex = e.NewPageIndex;
            DGVPayForm.DataSource = Session["dtData"];
            DGVPayForm.DataBind();
        }

        protected void DGVPayForm_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void DGVPayForm_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DGVPayForm.EditIndex = e.NewEditIndex;

        }

        protected void DGVPayForm_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void DGVPayForm_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void DGVPayForm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                // Label SSid = (Label)GVListShow.Rows[e.RowIndex].FindControl("lblExpId");
                EditRecord(Convert.ToInt32(e.CommandArgument));
                Session["edit"] = 1;
                Session["pram"] = e.CommandArgument;
            }

            if (e.CommandName == "Delete")
            {

                Cls_Login login = (Cls_Login)Session["login"];
                BudgetCMOEntities ent = new BudgetCMOEntities();
                CbDelete_Logs log = new CbDelete_Logs();
                int expid = Convert.ToInt32(e.CommandArgument);
                var logs = (from a in ent.expenses where a.exp_id == expid select a).First();
                log.cbno = logs.cbno;
                log.fyear = logs.fyear;
                log.amount = logs.amount;
                log.username = login.Username;
                log.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                ent.CbDelete_Logs.Add(log);
                ent.expenses.Remove(logs);
                var delag = (from a in ent.ags where a.cbno == logs.cbno && a.fyear == login.fyear select a).ToList();
                if (delag.Count() > 0)
                {
                    var agdelete = delag.First();
                    ent.ags.Remove(agdelete);
                }
                ent.SaveChanges();
                LoadList();
            }

            if (e.CommandName == "Cancel")
            {
                Response.Redirect("../DashBoard.aspx");
            }

        }

        protected void DDLHead_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadList();
        }

        protected void DGVPayForm_PageIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}