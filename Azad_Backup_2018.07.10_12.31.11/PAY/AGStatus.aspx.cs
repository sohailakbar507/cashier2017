﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CashierModel;

namespace Azad.Pay
{
    public partial class AGStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // this.txtNetClaim.Attributes.Add("onkeyup", "OnFiguresChange()");
            if (!IsPostBack)
            {
                LoadStatus();

            }

        }

        private void LoadStatus()
        {
            BudgetCMOEntities bg = new BudgetCMOEntities();
            var stat = (from a in bg.statcodes select a).ToList();
            DDLStatus.DataSource = stat;
            DDLStatus.DataValueField = "StatId";
            DDLStatus.DataTextField = "desc";
            DDLStatus.DataBind();
        }

        private void EditRecord()
        {
            BudgetCMOEntities bd = new BudgetCMOEntities();
            int agid = Convert.ToInt32(Request["ag_id"]);
            var agstatus = (from a in bd.ags
                where a.ag_id == agid
                select new { a.ag_id, a.cbno, a.totamount, a.tax, a.netclaim, a.dosubtoag, a.tokenno, a.doobj, a.status, a.dochqrec, a.chamount, a.chqno, a.remarks }).ToList();
            if (agstatus.Count() > 0)
            {
                var abc = agstatus.First();
                txtCBNo.Text = abc.cbno;

            }
        }
        
        protected void btnAdd_Click(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            if (Session["edit"] == null)
            {
                Cls_Login login = (Cls_Login)Session["Login"];
                if (Convert.ToDateTime(DateTime.Now.ToShortDateString()) >= login.yearstart && Convert.ToDateTime(DateTime.Now.ToShortDateString()) <= login.yearend)
                {

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Date is invalid please check date and try again.');", true);
                    return;
                }

                ag agt = new ag();
                agt.cbno = txtCBNo.Text;

                agt.totamount = txtCBAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtCBAmount.Text);
                agt.tax = txtTax.Text == string.Empty ? 0 : Convert.ToDouble(txtTax.Text);
                agt.netclaim = txtNetClaim.Text == string.Empty ? 0 : Convert.ToDouble(txtNetClaim.Text);
                agt.amtinwordn = txtAmntinWords.Text;
                //agt.dosubtoag = RDSubtoAG.SelectedDate;
                agt.tokenno = txtTokenNo.Text;
                //agt.doobj = RDObjection.SelectedDate;
                agt.status = Convert.ToInt32(DDLStatus.SelectedItem.Value);
                //agt.dochqrec = RDChqDate.SelectedDate;
                agt.chamount = txtAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtAmount.Text);
                agt.fyear = login.fyear;
                agt.remarks = txtRemarks.Text;
                cmo.ags.Add(agt);
                cmo.SaveChanges();

            }

            else
            {
                int agid = Convert.ToInt32(Request["ag_id"]);
                ag AG = (from a in cmo.ags where a.ag_id == agid select a).First();
                AG.cbno = txtCBNo.Text;
                AG.totamount = txtCBAmount.Text == string.Empty ? 0 : Convert.ToDouble(txtCBAmount.Text);
                AG.tax = txtTax.Text == string.Empty ? 0 : Convert.ToDouble(txtTax.Text);
                AG.netclaim = txtNetClaim.Text == string.Empty ? 0 : Convert.ToDouble(txtNetClaim.Text);
                AG.amtinwordn = txtAmntinWords.Text;
                if (RDSubtoAG.DateInput.SelectedDate != null)
                {
                   // AG.dosubtoag = RDSubtoAG.DateInput.SelectedDate;
                }

                AG.tokenno = txtTokenNo.Text;

                if (RDObjection.DateInput.SelectedDate != null)
                {
                    //AG.doobj = RDObjection.DateInput.SelectedDate;
                }

                AG.status = Convert.ToInt32(DDLStatus.SelectedItem.Value);
                
                if (RDChqDate.DateInput.SelectedDate != null)
                {
                    //AG.dochqrec = RDChqDate.DateInput.SelectedDate;
                }

                AG.chamount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                AG.remarks = txtRemarks.Text;
                cmo.SaveChanges();

               
                
            }
   
       }
    }
}