﻿/// <summary>
/// This class is used for JSON serialization and deserialization. It is mapped to and from the Product entity class
/// </summary>
using System.Runtime.Serialization;
[KnownType(typeof(ProductViewModel))]
public class ProductViewModel
{
    public int? partycode { get; set; }
    public string mbno  { get; set; }
    public string name { get; set; }
    public string address  { get; set; }
}