﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class StimualReportsForm : System.Web.UI.Page
    {
        private const string Correctionform = "Correction Form";
        private const string VERIFICATION = "Verification";
        private const string TELEPHONE_BILLS = "Telephone Bills";
        private const string BUDGET_ALLOCATION = "Budget Allocation";
        private const string SANCTION = "Sanction";
        private const string BILL_FORM = "Bill Form";
        private const string COMPUTER_SHEET = "Computer Sheet";
        private const string ALL_ABOVE = "All Above";
        private const string SANCTION_BY_SO = "Sanction by SO";
        private const string CHANGE_SHEET = "Change Sheet";
        private const string SPECIAL_ALLOWANCE = "Special Allowance";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    List<string> menuOptions = new List<string>();
                    menuOptions.Add(Correctionform);
                    menuOptions.Add(VERIFICATION);
                    menuOptions.Add(TELEPHONE_BILLS);
                    menuOptions.Add(BUDGET_ALLOCATION);
                    menuOptions.Add(SANCTION);
                    menuOptions.Add(BILL_FORM);
                    menuOptions.Add(COMPUTER_SHEET);
                    menuOptions.Add(ALL_ABOVE);
                    menuOptions.Add(SANCTION_BY_SO);
                    menuOptions.Add(CHANGE_SHEET);
                    menuOptions.Add(SPECIAL_ALLOWANCE);
                    rblReportCategories.DataSource = menuOptions;
                    rblReportCategories.DataBind();
                    //this.txtYear.Text = (DateTime.Now.Year).ToString() + "-" + (DateTime.Now.Year +1).ToString();
                    Cls_Login login = (Cls_Login)Session["Login"];
                    txtYear.Text = login.fyear;

                    using (BudgetCMOEntities db = new BudgetCMOEntities())
                    {

                        if (login.Groupid != 1)
                        {
                            var cbList = (from x in db.expenses
                                          where x.fyear == login.fyear && x.UserName == login.Username
                                          select new { x.cbno }).Distinct().ToList();

                            this.rcbCBNo.DataSource = cbList;
                            this.rcbCBNo.DataTextField = "cbno";
                            this.rcbCBNo.DataValueField = "cbno";
                            this.rcbCBNo.DataBind();
                            if (Session["cb"] != null)
                            {
                                rcbCBNo.SelectedValue = Session["cb"].ToString();
                            }

                        }
                        else
                        {
                            var cbList = (from x in db.expenses
                                          where x.fyear == login.fyear
                                          select new { x.cbno }).Distinct().ToList();

                            this.rcbCBNo.DataSource = cbList;
                            this.rcbCBNo.DataTextField = "cbno";
                            this.rcbCBNo.DataValueField = "cbno";
                            this.rcbCBNo.DataBind();
                            if (Session["cb"] != null)
                            {
                                rcbCBNo.SelectedValue = Session["cb"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                { Response.Write(ex); }
            }
            else
                this.ProcessReports();
        }

        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RadioButton btn = (RadioButton) sender;


            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "Correction Form":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("UtilityBillsVerification.aspx?cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Verification":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=Verification&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Telephone Bills":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=TELEPHONEBills&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Budget Allocation":
                    Session["cb"] = rcbCBNo.Text;
                    rblReportCategories.SelectedValue = null;
                    // Response.Redirect("ViewReports.aspx?reportid=bujallocation&cbno=" + rcbCBNo.Text.Trim() + "&fyear=" + txtYear.Text.Trim());
                    AllocateBudget();
                    break;
                case "Bill Form":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=BillForm&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case "Sanction":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=Sanction&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Computer Sheet":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=ComputerSheet&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Change Sheet":
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewSTIReports.aspx?reportid=ChangeSheet&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
                case ALL_ABOVE:
                    Session["cb"] = rcbCBNo.Text;
                    Response.Redirect("ViewCombinedReports.aspx?reportid=AllAbove&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);
                    break;
            }

        }

        private void AllocateBudget()
        {
            BudgetCMOEntities dg = new BudgetCMOEntities();
            ObjectParameter op = new ObjectParameter("status", "");
            dg.BudgetAllocationComplete(rcbCBNo.Text.Trim(), txtYear.Text.Trim(), op);
            if (op.Value.ToString() == "Budget allocated successfully")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + op.Value.ToString() + "');", true);
            }
            else
            
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + op.Value.ToString() + "');", true);
            }
            //Response.Write(op.Value.ToString());
        }

        protected void ProcessReports()
        {
            //if (string.IsNullOrEmpty(this.rblReportCategories.SelectedValue))
            //{
            //    this.lblError.Text = "Please Select any Report";
            //    return;
            //}

            //if (string.IsNullOrEmpty(this.rcbCBNo.Text))
            //{
            //    this.lblError.Text = "Please Enter CB #";
            //    return;
            //}

            //if (string.IsNullOrEmpty(this.txtYear.Text))
            //{
            //    this.lblError.Text = "Please Enter Year";
            //    return;
            //}
            // BudgetCMOEntities dbcontext = new BudgetCMOEntities();

            //  string cb = this.rcbCBNo.Text;
            //string year = this.txtYear.Text;

            //switch (this.rblReportCategories.SelectedValue)
            //{
            //    case BUDGET_ALLOCATION:
            //        ObjectParameter opStatus = new ObjectParameter("status", "");
            //        dbcontext.BudgetAllocationComplete(cb, year, opStatus);
            //        this.lblError.Text = opStatus.Value.ToString();
            //        break;
            //    default:
            //        this.lblError.Text = "Report Not Implemented Yet";
            //        break;
            //}
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("UtilityBillsVerification.aspx");
        }
    }
}