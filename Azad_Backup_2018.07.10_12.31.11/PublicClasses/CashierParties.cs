﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azad.PublicClasses
{
    public class CashierParties
    {
        public int partyid { get; set; }
        public Nullable<int> partycode { get; set; }
        public Nullable<int> offcode { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string phno { get; set; }
        public string mbno { get; set; }
        public string faxno { get; set; }
        public string nature { get; set; }
        public string section { get; set; }
    }
}