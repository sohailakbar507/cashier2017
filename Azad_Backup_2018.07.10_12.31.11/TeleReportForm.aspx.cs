﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class TeleReportForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Cls_Login login = (Cls_Login)Session["login"];
                txtFYear.Text = login.fyear;
            }
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewSTIReports.aspx?reportid=TELEPHONEBills&cbno=" + txtCBNo.Text + "&fyear=" + txtFYear.Text);
        }
    }
}