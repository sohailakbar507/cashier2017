﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="LoginForm.aspx.cs" Inherits="Azad.LoginForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

.footer {
  position:absolute;
  bottom: 0;
  left:0px;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  color: #FFFFFF;
  background-color: #5cb85c;
  text-align:center;
}

/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */
</style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                             <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
                            <fieldset>
                                <div class="form-group">
                                   <input class="form-control" placeholder="Username" runat="server" id="txtusername" type="text" required="required" autofocus="autofocus" />
       
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" runat="server"  id="txtPass" type="password" required="required" />
                                   
      
                                </div>
                                <div class="form-group">
                                    <telerik:RadComboBox ID="rcbyears" Width="100%" Runat="server" Filter="StartsWith" CssClass="form-control">
                        </telerik:RadComboBox>
                                </div>
                                <div class="checkbox">
                                   
                                        <a href="ChangePassword.aspx">Change Password</a>
                                   
                                </div>
                                 <asp:Button ID="btnSubmit" runat="server" Text="Login" 
              CssClass="btn btn-lg btn-success btn-block" 
            onclick="btnSubmit_Click" />
                            </fieldset>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- jQuery -->
     <footer class="footer">
      <div class="container">
        <p class="text-muted" style="color:#fff">Designed and Developed by Sohail Akbar, Assistant Programmer,</p>
        <p class="text-muted" style="color:#fff"> Chief Minister's Office, 8-Club Road, Lahore </p>
      </div>
    </footer>

</asp:Content>
