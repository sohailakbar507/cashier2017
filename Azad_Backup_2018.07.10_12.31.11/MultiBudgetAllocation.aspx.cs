﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class MultiBudgetAllocation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["login"] != null)
                {
                    LoadRecords();
                }
            }

        }
        //public static string PadNumbers(string input)
        //{
        //    return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        //}
        private void LoadRecords()
        {
            //MultiDatePicker.Visible = false;
            //int catid = Convert.ToInt32(Request.QueryString["catid"]);
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            Cls_Login login = (Cls_Login)Session["login"];
            DateTime Fromdate = Convert.ToDateTime(MultiDatePicker1.DateInput.SelectedDate);
            var abc = (from a in cmo.expenses
            where a.fyear == login.fyear && a.UserName == login.Username
            orderby a.cbno
            //orderby int.Parse(a.cbno.Substring(2,6))
            select new { a.exp_id,a.date,a.head,a.cbno,a.amount,a.fyear}).Take(5).ToList();
            
            DGVMultiBudget.DataSource = abc;
            DGVMultiBudget.DataBind();
            Session["dtData"] = abc;
        }

        protected void lnkBudget_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities bill = new BudgetCMOEntities();
            foreach (GridViewRow row in DGVMultiBudget.Rows)
            {
                //TextBox pifra = (TextBox)row.FindControl("txtPifra");
                Label cb = (Label)row.FindControl("lblcbno");
                Label fyear = (Label)row.FindControl("lblfyear");
                ObjectParameter op = new ObjectParameter("status", "");
                bill.BudgetAllocationComplete(cb.Text.Trim(), fyear.Text.Trim(), op);
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + "Budget Successfully Allocated" + "');", true);
        }

        protected void lnkShowData_Click(object sender, EventArgs e)
        {
           // MultiDatePicker.Visible = true;
            BudgetCMOEntities cmo = new BudgetCMOEntities();
            Cls_Login login = (Cls_Login)Session["login"];
            DateTime Fromdate = Convert.ToDateTime(MultiDatePicker1.DateInput.SelectedDate);
            var abc = (from a in cmo.expenses
                       where a.fyear == login.fyear && a.UserName == login.Username && a.date == Fromdate
                       //orderby int.Parse( a.cbno.Substring(3))
                       select new { a.exp_id, a.date, a.head, a.cbno, a.amount, a.fyear }).ToList();

            var sorted_abc = (from a in abc
                              orderby int.Parse(a.cbno.Substring(3))
                              select a
                ).ToList();
            DGVMultiBudget.DataSource = sorted_abc;
            DGVMultiBudget.DataBind();
            Session["dtData"] = sorted_abc;

        }

        protected void LinkPrint_Click(object sender, EventArgs e)
        {
           
        }
    }
}