﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class PayReportsForm : System.Web.UI.Page
    {
        private const string PAY_REPORT = "Pay Report";
        private const string PAY_Bill = "Pay Bill";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!this.IsPostBack)
            {
                Cls_Login login = (Cls_Login)Session["Login"];
                List<string> menuOptions = new List<string>();
                menuOptions.Add(PAY_REPORT);
                menuOptions.Add(PAY_Bill);
                rblReportCategories.DataSource = menuOptions;
                rblReportCategories.DataBind();
                this.txtYear.Text = login.fyear;

                using (BudgetCMOEntities db = new BudgetCMOEntities())
                {
                    var cbList = (from x in db.expenses
                                  where x.fyear == login.fyear && x.UserName == login.Username
                                  select new { x.cbno}).Distinct().ToList();

                    this.rcbCBNo.DataSource = cbList;
                    this.rcbCBNo.DataTextField = "cbno";
                    this.rcbCBNo.DataValueField = "cbno";
                    this.rcbCBNo.DataBind();

                }
            }
            else
                this.ProcessReports();
        }
        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RadioButton btn = (RadioButton) sender;


            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "Pay Report":
                    Response.Redirect("ViewPayReport.aspx?reportid=PayReport&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
                case "Pay Bill":
                    Response.Redirect("ViewPayReport.aspx?reportid=PayBill&cbno=" + rcbCBNo.Text + "&fyear=" + txtYear.Text);

                    break;
            }
        }
             protected void ProcessReports()
        {
            if (string.IsNullOrEmpty(this.rblReportCategories.SelectedValue))
            {
                this.lblError.Text = "Please Select any report";
                return;
            }

            if (string.IsNullOrEmpty(this.rcbCBNo.Text))
            {
                this.lblError.Text = "Please enter CB #";
                return;
            }

            if (string.IsNullOrEmpty(this.txtYear.Text))
            {
                this.lblError.Text = "Please Enter Year";
                return;
            }
            BudgetCMOEntities dbcontext = new BudgetCMOEntities();

            string cb = this.rcbCBNo.Text;
            string year = this.txtYear.Text;

            switch (this.rblReportCategories.SelectedValue)
            {
                    default:
                    this.lblError.Text = "Report not implemented yet";
                    break;
            }
        }

    }
}
