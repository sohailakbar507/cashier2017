﻿using Aspose.Cells;
using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class AgGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LicenseHelper.ModifyInMemory.ActivateMemoryPatching();
                UploadAGdataGrid();
            }
        }

        private void UploadAGdataGrid()
        {
            BudgetCMOEntities ag = new BudgetCMOEntities();
            var agdata = (from a in ag.AGDataSums
                          select new { a.cbno, a.offcode, a.sum_amount}).ToList();
            AGdataGridView.DataSource = agdata;
            AGdataGridView.DataBind();
        }

        protected void AGdataGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AGdataGridView.PageIndex = e.NewPageIndex;
            AGdataGridView.DataSource = Session["AGData"];
            AGdataGridView.DataBind();
        }

        protected void AGdataGridView_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities ag = new BudgetCMOEntities();
            foreach (GridViewRow row in AGdataGridView.Rows)
            {
                TextBox cb = (TextBox)row.FindControl("txtSum_Amount");
                int sumid = Convert.ToInt32(AGdataGridView.DataKeys[row.RowIndex].Value);
                AGDataSum vdata = new AGDataSum();
                AGDataSum update = (from a in ag.AGDataSums where a.offcode == sumid select a).First();
                update.sum_amount = Convert.ToDouble(cb.Text);
                ag.SaveChanges();

            }
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities ag = new BudgetCMOEntities();
            var agdata = (from a in ag.AGDataSums 
                          select new { a.cbno, a.offcode, a.sum_amount }).ToList();

            Workbook workbook = new Workbook();
            Worksheet sheet = workbook.Worksheets[0];
            sheet.Cells.ImportCustomObjects(agdata.ToArray(), new string[] { "cbno", "offcode", "Sum_amount"},true,0,0,agdata.Count,true,"dd/mm/yyyy",false);
            sheet.AutoFitColumns();
            workbook.Save(this.Response, "AgData.xls", ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Excel97To2003));
            Response.End();
        }
    }

 
}