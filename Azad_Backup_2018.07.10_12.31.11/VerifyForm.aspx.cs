﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class VerifyForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Calculate();
            }
        }

        private void Calculate()
        {
            BudgetCMOEntities ent = new BudgetCMOEntities();
            int month = Convert.ToInt32(Session["month"]);
            int year = Convert.ToInt32(Session["year"]);
            string head = Session["head"].ToString();

            var cb1 = (from a in ent.expenses
                       where a.dobill.Value.Year == year
                       && a.dobill.Value.Month == month
                       && a.head == head
                       && a.offid != 88
                       && !(a.descr.StartsWith("3"))
                       select a.amount).Sum();
            var cb2 = (from a in ent.expenses where a.dobill.Value.Year == year && a.dobill.Value.Month == month && a.head == head && a.offid == 88 select a.amount).Sum();
            var cb3 = (from a in ent.expenses where a.dobill.Value.Year == year && a.dobill.Value.Month == month && a.head == head && (a.descr.StartsWith("3")) select a.amount).Sum();

            lblTotalNTC.Text = cb1.ToString();
            lblTotalIslamabad.Text = cb2.ToString();
            lblTotalPTCL.Text = cb3.ToString();

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Telephone.aspx");

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities ent = new BudgetCMOEntities();
            int month = Convert.ToInt32(Session["month"]);
            int year = Convert.ToInt32(Session["year"]);
            string head = Session["head"].ToString();

            var cb1 = (from a in ent.expenses
                       where a.dobill.Value.Year == year
                       && a.dobill.Value.Month == month
                       && a.head == head
                       && a.offid != 88
                       && !(a.descr.StartsWith("3"))
                       select a).ToList();
            var cb2 = (from a in ent.expenses where a.dobill.Value.Year == year && a.dobill.Value.Month == month && a.head == head && a.offid == 88 select a).ToList();
            var cb3 = (from a in ent.expenses where a.dobill.Value.Year == year && a.dobill.Value.Month == month && a.head == head && (a.descr.StartsWith("3")) select a).ToList();

            if (cb1.Count() > 0)
            {
                foreach (var updatecb in cb1)
                {
                    if (updatecb.cbno == "" || updatecb.cbno == null)
                    {
                        updatecb.cbno = txtCB.Text;
                        updatecb.partycode = 44;
                        ent.SaveChanges();
                    }
                }
            }

            if (cb2.Count() > 0)
            {
                foreach (var updatecb in cb2)
                {
                    if (updatecb.cbno == "" || updatecb.cbno == null)
                    {
                        updatecb.cbno = txtCBIDB.Text;
                        updatecb.partycode = 45;
                        ent.SaveChanges();
                    }
                }
            }

            if (cb3.Count() > 0)
            {
                foreach (var updatecb in cb3)
                {
                    if (updatecb.cbno == "" || updatecb.cbno == null)

                        updatecb.cbno = txtPTCL.Text;
                    updatecb.partycode = 47;
                    ent.SaveChanges();

                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeleReportForm.aspx");
        }


    }
}

 