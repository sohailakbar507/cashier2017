﻿using CashierModel;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Base.Design;

namespace Azad
{
    public partial class ViewReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                this.LoadReport();
            }
        }


        private void LoadReport()
        {
            BudgetCMOEntities dg = new BudgetCMOEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            //var getrptid = (from a in dg.ReportsTables where a.ReportName == rptid select a).First();
            //var getrptacess = from a in dg.ReportAccesses where a.UserId == loggedInUser.Userid && a.ReportId == getrptid.ReportId select a;
            //if (getrptacess.Count() > 0)
            //{
            //    var accessssssss = getrptacess.First();
            //    if (accessssssss.Access == true)
            //    {
            //        access = true;
            //    }
            //    else
            //    {
            //        access = false;
            //    }
            //}
            //else
            //{

            //}

            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("../LoginPage.aspx");
                return;
            }

            if (rptid == "UtilityBills")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.proc_VerifyCb(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/UTILITYBILLSREPO.rdlc");
            }
            if (rptid == "TELEPHONEBills")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.proc_VerifyCb(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/VerifyCBRepo.rdlc");
            }
            if (rptid == "bujallocation")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                ObjectParameter op = new ObjectParameter("status","");
                dg.BudgetAllocationComplete(cb, fyear, op);
                
                Response.Write(op.Value.ToString());
            }
            if (rptid == "Sanction")              
            {
                string cbno = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                StiReport rpt = new StiReport();
                rpt["cbno"] = cbno;
                rpt["fyear"] = fyear;
                string path = Server.MapPath("~/CashierReports/Sanction.mrt");
                rpt.Load(path);
                rpt.Render();
                
            }
            if (rptid == "BillForm")              
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.BillForm(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/BillFormReport.rdlc");
            }
            if (rptid == "ComputerSheet")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.ComputerSheet_Result1(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/ComputerSheet.rdlc");
            }
            if (rptid == "CBSheet")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.CbSheetProc(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/rptCBSheet.rdlc");
            }

            if (rptid == "PayReports")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.PayForm_Result(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/PayBill.rdlc");
                
            }
            if (rptid == "PayBill2nd")
            {
                string cb = Request.QueryString["cbno"];
                string fyear = Request.QueryString["fyear"];
                repo = dg.PayForm_Result(cb, fyear).ToList();
                report_path = Server.MapPath("~/CashierReports/PayBill2nd.rdlc");

            }

            if (access == true)
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = report_path;
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", repo));
                ReportViewer1.LocalReport.Refresh();
            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }
        }
    }
}

 