﻿using CashierModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Azad
{
    public partial class Telephone : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //UploadGrid(date);
            }


        }


        private void UploadGrid(int month, int year)
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            var log = (Cls_Login)Session["Login"];
            var tele = (from a in tel.teles
                        select new { a.phno, a.offid, a.amount }).ToList();

            var chk = (from a in tel.expenses
                       where a.dobill.Value.Month == month && a.dobill.Value.Year == year && a.descr!=null && a.head == "A03202000" && a.offid != 88
            && !(a.descr.StartsWith("3"))
                       orderby a.descr
                       select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            if (chk.Count() > 0)
            {
                //var data1 = (from a in tel.expenses where a.dobill.Value.Month == month && a.dobill.Value.Year == year select a).ToList();
                DGVTelephone.DataSource = chk;
                DGVTelephone.DataBind();
                return;
            }
            else
            { }
            foreach (var abc in tele)
            {

                expense rec = new expense();
                rec.offid = Convert.ToInt32(abc.offid);
                rec.amount = 0;
                rec.descr = abc.phno;
                var lastday = DateTime.DaysInMonth(RadMonthYearPicker1.SelectedDate.Value.Year, RadMonthYearPicker1.SelectedDate.Value.Month);
                rec.dobill = Convert.ToDateTime(RadMonthYearPicker1.SelectedDate.Value.Year + "-" + RadMonthYearPicker1.SelectedDate.Value.Month + "-" + lastday);
                rec.head = "A03202000";
                rec.UserName = "Shabbir";
                v_AdminStaff admn = (from x in tel.v_AdminStaff where x.EmployeeId == abc.offid select x).FirstOrDefault();
                rec.name = admn == null ? "" : admn.EmployeeName + ", " + admn.Desg;
                rec.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                rec.fyear = log.fyear;
                tel.expenses.Add(rec);
                tel.SaveChanges();

            }

            var data = (from a in tel.expenses
                        where a.dobill.Value.Month == month && a.descr!=null && a.dobill.Value.Year == year && a.head == "A03202000" && a.offid != 88
                        && !(a.descr.StartsWith("3"))
                        orderby a.descr
                        select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            DGVTelephone.DataSource = data;
            DGVTelephone.DataBind();
        }
        private void UploadGridIDB(int month, int year)
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            var tele = (from a in tel.teles
                        select new { a.phno, a.offid, a.amount }).ToList();

            var chk = (from a in tel.expenses
                       where a.dobill.Value.Month == month && a.dobill.Value.Year == year && a.head == "A03202000" && a.offid == 88 && a.descr != null
                       orderby a.descr
                       select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            if (chk.Count() > 0)
            {
                //var data1 = (from a in tel.expenses where a.dobill.Value.Month == month && a.dobill.Value.Year == year select a).ToList();
                DGVTelephone.DataSource = chk;
                DGVTelephone.DataBind();
                return;
            }
            else
            { }
            foreach (var abc in tele)
            {

                expense rec = new expense();
                rec.offid = Convert.ToInt32(abc.offid);
                rec.amount = 0;
                rec.descr = abc.phno;
                var lastday = DateTime.DaysInMonth(RadMonthYearPicker1.SelectedDate.Value.Year, RadMonthYearPicker1.SelectedDate.Value.Month);
                rec.dobill = Convert.ToDateTime(RadMonthYearPicker1.SelectedDate.Value.Year + "-" + RadMonthYearPicker1.SelectedDate.Value.Month + "-" + lastday);
                rec.head = "A03202000";
                rec.UserName = "Shabbir";
                v_AdminStaff admn = (from x in tel.v_AdminStaff where x.EmployeeId == rec.offcode select x).First();
                rec.name = admn == null ? "" : admn.EmployeeName + ", " + admn.Desg;
                rec.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                var log = (Cls_Login)Session["Login"];
                rec.fyear = log.fyear;
                tel.expenses.Add(rec);
                tel.SaveChanges();

            }

            var data = (from a in tel.expenses
                        where a.dobill.Value.Month == month && a.dobill.Value.Year == year && a.head == "A03202000" && a.offid == 88 
                        select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            DGVTelephone.DataSource = data;
            DGVTelephone.DataBind();
        }
        private void UploadGridPTCL(int month, int year)
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            var tele = (from a in tel.teles
                        select new { a.phno, a.offid, a.amount }).ToList();

            var chk = (from a in tel.expenses
                       where a.dobill.Value.Month == month && a.dobill.Value.Year == year && a.head == "A03202000" && (a.descr.StartsWith("3")) && a.descr != null
                       orderby a.descr
                       select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            if (chk.Count() > 0)
            {
                //var data1 = (from a in tel.expenses where a.dobill.Value.Month == month && a.dobill.Value.Year == year select a).ToList();
                DGVTelephone.DataSource = chk;
                DGVTelephone.DataBind();
                return;
            }
            else
            { }
            foreach (var abc in tele)
            {

                expense rec = new expense();
                rec.offid = Convert.ToInt32(abc.offid);
                rec.amount = 0;
                rec.descr = abc.phno;
                var lastday = DateTime.DaysInMonth(RadMonthYearPicker1.SelectedDate.Value.Year, RadMonthYearPicker1.SelectedDate.Value.Month);
                rec.dobill = Convert.ToDateTime(RadMonthYearPicker1.SelectedDate.Value.Year + "-" + RadMonthYearPicker1.SelectedDate.Value.Month + "-" + lastday);
                rec.head = "A03202000";
                rec.UserName = "Shabbir";
                v_AdminStaff admn = (from x in tel.v_AdminStaff where x.EmployeeId == rec.offcode select x).First();
                rec.name = admn == null ? "" : admn.EmployeeName + ", " + admn.Desg;
                rec.date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                var log = (Cls_Login)Session["Login"];
                rec.fyear = log.fyear;
                tel.expenses.Add(rec);
                tel.SaveChanges();

            }

            var data = (from a in tel.expenses
                        where a.dobill.Value.Month == month && a.dobill.Value.Year == year && a.head == "A03202000" && (a.descr.StartsWith("3"))
                        orderby a.descr
                        select new { a.exp_id, a.descr, a.offid, a.amount }).ToList();
            DGVTelephone.DataSource = data;
            DGVTelephone.DataBind();
        }

        protected void DGVTelephone_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void DGVTelephone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVTelephone.PageIndex = e.NewPageIndex;
            DGVTelephone.DataSource = Session["TeleData"];
            DGVTelephone.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BudgetCMOEntities tel = new BudgetCMOEntities();
            int year = RadMonthYearPicker1.SelectedDate.Value.Year;
            int month = RadMonthYearPicker1.SelectedDate.Value.Month;
            foreach (GridViewRow row in DGVTelephone.Rows)
            {
                TextBox cb = (TextBox)row.FindControl("txtAmountUpdated");
                int sumid = Convert.ToInt32(DGVTelephone.DataKeys[row.RowIndex].Value);
                expense exp = new expense();
                expense update = (from a in tel.expenses where a.exp_id == sumid select a).First();
                update.amount = Convert.ToDouble(cb.Text);
                //update.fyear = (Convert.ToInt32(DateTime.Now.Year) + "-" + Convert.ToInt32(DateTime.Now.Year + 1)).ToString();
                tel.SaveChanges();

            }
        }

        protected void DGVTelephone_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void RadMonthYearPicker1_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {

        }

        protected void ViewDetail_Click(object sender, EventArgs e)
        {
            try
            {
                int year = RadMonthYearPicker1.SelectedDate.Value.Year;
                int month = RadMonthYearPicker1.SelectedDate.Value.Month;
                UploadGrid(month, year);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                Session["month"] = RadMonthYearPicker1.SelectedDate.Value.Month;
                Session["year"] = RadMonthYearPicker1.SelectedDate.Value.Year;
                Session["head"] = "A03202000";
                //Response.Redirect("VerifyForm.aspx?month="+RadMonthYearPicker1.SelectedDate.Value.Month+"and year="+RadMonthYearPicker1.SelectedDate.Value.Year+"and head=A03202000");
                Response.Redirect("VerifyForm.aspx");
            }
            catch (Exception ex)
            { }
        }

        protected void btnIDB_Click(object sender, EventArgs e)
        {
            try
            {
                int year = RadMonthYearPicker1.SelectedDate.Value.Year;
                int month = RadMonthYearPicker1.SelectedDate.Value.Month;
                UploadGridIDB(month, year);
            }
            catch
            {

            }
        }

        protected void btnPTCL_Click(object sender, EventArgs e)
        {
            try
            {
                int year = RadMonthYearPicker1.SelectedDate.Value.Year;
                int month = RadMonthYearPicker1.SelectedDate.Value.Month;
                UploadGridPTCL(month, year);
            }
            catch(Exception ex)
            {

            }
        }

    }
}
